import math

import numpy as np

int16max = 32768
gyroSpectrum = 500
accSpectrum = 2
magnSpectrum = 8

def checkIfMessageIsCorrect(msg):
    if len(msg) < 26:
        return False

    # elif msg[0] != b'$':
    #     return False
    #
    # elif msg[25] != b'(':
    #     return False
    else:
        return True

def decodeMessage(msg):

     if checkIfMessageIsCorrect(msg):

         gyrox = np.int16(msg[1] + (msg[2] << 8)) / int16max * gyroSpectrum * math.pi / 180
         gyroy = np.int16(msg[3] + (msg[4] << 8)) / int16max * gyroSpectrum * math.pi / 180
         gyroz = np.int16(msg[5] + (msg[6] << 8)) / int16max * gyroSpectrum * math.pi / 180

         accx = np.int16(msg[7] + (msg[8] << 8)) / int16max * accSpectrum
         accy = np.int16(msg[9] + (msg[10] << 8)) / int16max * accSpectrum
         accz = np.int16(msg[11] + (msg[12] << 8)) / int16max * accSpectrum

         magnx = np.int16(msg[13] + (msg[14] << 8)) / int16max * magnSpectrum
         magny = np.int16(msg[15] + (msg[16] << 8)) / int16max * magnSpectrum
         magnz = np.int16(msg[17] + (msg[18] << 8)) / int16max * magnSpectrum

         time = (msg[23] + (msg[24] << 8))

         print(accz)

         return True

     else:
         return False