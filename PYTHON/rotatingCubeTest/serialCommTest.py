import serial
import time
from messageReader import decodeMessage

def synchronize():
    c = ' '
    while c != b'(':
        c = serialPort.read(1)

serialPort = serial.Serial(
    port="COM8", baudrate=1000000, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE
)
serialString = ""  # Used to hold data coming over UART
while 1:
    # Wait until there is data waiting in the serial buffer
    if serialPort.in_waiting > 0:

        # Read data out of the buffer until a carraige return / new line is found
        #serialString = serialPort.readline()

        synchronize()

        res = serialPort.read(26)

        result = decodeMessage(res)

        # try:
        #     #print(res)
        #     #print(res.decode("Ascii"))
        # except:
        #     pass