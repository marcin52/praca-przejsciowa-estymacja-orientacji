import serial
from messageReader import decodeMessage
import threading

from pcRotatingCubeApp.angleCalculator import AngleCaluclator


def sample(gx, gy, gz, ax, ay, az, mx, my, mz, time):
    pass

def synchronize(serialPort):

    c = ' '
    while c != b'(':
        c = serialPort.read(1)

def readData(comPort, updateFunction):

    serialPort = serial.Serial(
        port=comPort, baudrate=1000000, bytesize=8, timeout=2, stopbits=serial.STOPBITS_ONE
    )
    serialString = ""  # Used to hold data coming over UART

    while 1:
        # Wait until there is data waiting in the serial buffer
        if serialPort.in_waiting > 0:
            # Read data out of the buffer until a carraige return / new line is found
            # serialString = serialPort.readline()

            synchronize(serialPort)

            res = serialPort.read(26)

            result = decodeMessage(res, updateFunction)

if __name__ == '__main__':
    calculator = AngleCaluclator(None)

    readData("COM8", calculator.calculate)