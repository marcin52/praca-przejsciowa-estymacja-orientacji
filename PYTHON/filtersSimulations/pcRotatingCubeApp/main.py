import threading
import numpy as np
import matplotlib.pyplot as plt
from matplotlib.widgets import Button, RadioButtons
from cubePlot import CubeAxes
from angleCalculator import AngleCaluclator
from serialCommTest import readData

if __name__ == '__main__':

    fig = plt.figure(0, figsize=(4, 4))
    ax = CubeAxes(fig)
    fig.add_axes(ax)
    ax.draw_cube()

    ax.rotateCube(np.pi, np.pi, np.pi)

    axStop = plt.axes([0.81, 0.06, 0.1, 0.075])
    axStart = plt.axes([0.7, 0.06, 0.1, 0.075])
    bStart = Button(axStart, 'Start')
    bStop = Button(axStop, 'Stop')

    axcolor = 'lightgoldenrodyellow'
    rax = plt.axes([0.05, 0.7, 0.15, 0.15], facecolor=axcolor)
    radio = RadioButtons(rax, ('DCM Kalman', 'Quaternion UKF', 'Mahony', 'Madgwick'))

    calculator = AngleCaluclator(ax)

    def hzfunc(label):
        pass
        # hzdict = {'2 Hz': s0, '4 Hz': s1, '8 Hz': s2}
        # ydata = hzdict[label]
        # l.set_ydata(ydata)
        # plt.draw()


    x = threading.Thread(target=readData, args=("COM8", calculator.calculate))
    def bStartFunc(event):
        #readData("COM8", calculator.calculate)
        x = threading.Thread(target=readData, args=("COM8", calculator.calculate))
        x.start()

    # def bStopFunc(event):
    #     x.

    radio.on_clicked(hzfunc)

    bStart.on_clicked(bStartFunc)
    plt.show()

