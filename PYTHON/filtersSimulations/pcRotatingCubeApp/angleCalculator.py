import sys
import threading
import math
import numpy as np

from helpers import rotation
from imu_filters import dspLib
from imu_filters.mahony import MahonyFilter
from imu_filters.madgwick import MadgwickFilter
from imu_filters.dcmKF import DCM_KF
from imu_filters.quaternionUKF2 import QuaternionUKF
from imu_filters.std_dev_filter import StdDevFilter
from helpers.magnetometer import Magnetometer
from imu_filters.accelerationCompensation import AccelComp

class AngleCaluclator():

    def __init__(self, cubePlot):
        self.magn_sigma = 0.8**2
        self.gyro_sigma = 0.3**2      #math.radians(0.2)
        self.gyro_bias = math.radians(10)
        self.acc_sigma = 0.5**2
        self.dt = 0.005
        self.timeCurrent = 0
        self.timePrevious = 0
        self.mahonyFilter = MahonyFilter(self.dt, 30, 0.0001, 0, 0, 0)
        self.madgwickFilter = MadgwickFilter(self.dt, math.sqrt(3 / 4) * 5, 0, 0, 0,
                                            math.sqrt(3 / 4) * self.gyro_bias)
        #self.madgwickFilter = MadgwickFilter(self.dt, 30, 0, 0, 0,1)
        self.dcm = DCM_KF(self.dt, self.gyro_sigma, self.acc_sigma)
        self.quatUKF = QuaternionUKF(self.dt, self.gyro_sigma, self.acc_sigma, self.magn_sigma,
                                     estimateBias=True, MARG_filt=True)



        self.lowPassX = None
        self.lowPassY = None
        self.lowPassZ = None

        self.highPassX = None
        self.highPassY = None
        self.highPassZ = None

        self.dev_gyrox = StdDevFilter()
        self.dev_gyroy = StdDevFilter()
        self.dev_gyroz = StdDevFilter()
        self.dev_accx = StdDevFilter()
        self.dev_accy = StdDevFilter()
        self.dev_accz = StdDevFilter()
        self.accelComp = AccelComp(self.acc_sigma, self.acc_sigma, self.acc_sigma, [0, 0, 1], 200, 10)

        self.magnCalib = Magnetometer()

        self.cubePlot = cubePlot

        self.counter = 0

        self.calibrationFinished = True

    def updateCube(self, roll, pitch, yaw):
        x = threading.Thread(target=self.cubePlot.rotateCube, args=(np.radians(roll), np.radians(pitch), np.radians(yaw)))
        x.start()

    def calculate(self, gx, gy, gz, ax, ay, az, mx, my, mz, time):

        if self.lowPassZ == None:

            self.lowPassX = dspLib.SimpleLowPassFilter(ax, 0.99)
            self.lowPassY = dspLib.SimpleLowPassFilter(ay, 0.99)
            self.lowPassZ = dspLib.SimpleLowPassFilter(az, 0.99)

            self.highPassX = dspLib.SimpleHighPassFilter(gx, 0.1)
            self.highPassY = dspLib.SimpleHighPassFilter(gy, 0.1)
            self.highPassZ = dspLib.SimpleHighPassFilter(gz, 0.1)

        if not self.calibrationFinished:
            self.calibrationFinished = self.magnCalib.calibrate(mx, my, mz)

        if self.counter == 0:
            qw, qx, qy, qz = rotation.getInitialQuaternion(ax, ay, az, mx, my, mz)

            self.quatUKF.ukf.x[0] = qw
            self.quatUKF.ukf.x[1] = qx
            self.quatUKF.ukf.x[2] = qy
            self.quatUKF.ukf.x[3] = qz

            self.madgwickFilter.q0 = qw
            self.madgwickFilter.q1 = qx
            self.madgwickFilter.q2 = qy
            self.madgwickFilter.q3 = qz

            self.counter = 1

        else:
            #hx, hy, hz = self.magnCalib.read_precalibrated3(mx, my, mz)
            #hx, hy, hz = mx, my, mz



            ax = self.lowPassX.filterStep(ax)
            ay = self.lowPassY.filterStep(ay)
            az = self.lowPassZ.filterStep(az)

            gx = self.highPassX.filterStep(gx)
            gy = self.highPassY.filterStep(gy)
            gz = self.highPassZ.filterStep(gz)

            R = self.accelComp.getR_Matrix(ax, ay, az)

            self.dcm.kf.R[3:6, 3:6] = R
            self.quatUKF.ukf.R[0:3, 0:3] = R

            # sys.stdout.write(f'\r{self.quatUKF.ukf.R[0]}')
            # sys.stdout.flush()

            #roll, pitch, yaw = self.madgwickFilter.simulationIMU_update(gx, gy, gz, ax, ay, az)
            #roll, pitch, yaw = self.mahonyFilter.simulationIMU_update(gx, gy, gz, ax, ay, az)
            #roll, pitch, yaw = self.dcm.updateIMU(gx, gy, gz, ax, ay, az)
            #roll, pitch, yaw = self.quatUKF.updateIMU(gx, gy, gz, ax, ay, az)

            roll, pitch, yaw = self.quatUKF.updateMARG(gx, gy, gz, ax, ay, az, mx, my, mz)
            #roll, pitch, yaw = self.madgwickFilter.simulationMARG_update(gx, gy, gz, ax, ay, az, mx, my, mz)
            #roll, pitch, yaw = self.mahonyFilter.simulationMARG_update(gx, gy, gz, ax, ay, az, mx, my, mz)

            #print(ax)

            if self.counter == 5:
                if self.cubePlot:
                    self.cubePlot.rotateCube(np.radians(pitch), np.radians(yaw), np.radians(roll))
                #self.cubePlot.rotateCube(np.radians(pitch), -np.radians(0), np.radians(roll))
                self.counter = 0
            else:
                self.counter+=1
            #print(f'{roll},{pitch},{yaw}')
            #print(f'{mx},{my},{mz}')
            #print(f'{gx},{gy},{gz}')
            #print(f'{ax},{ay},{az}')
            #self.updateCube(roll, pitch, yaw)

            sys.stdout.write(f'\r{roll},{pitch},{yaw}')
            sys.stdout.flush()

            self.timePrevious = time

