from helpers.magnetometer import Magnetometer

mx = []
my = []
mz = []

with open("cal_magn.txt", "r") as f:
    data = f.readlines()

    for index, line in enumerate(data):

        if index == 0:
            continue

        words = line.split()
        if words[0] == '\x00':
            continue

        mx.append(float(words[0]))
        my.append(float(words[1]))
        mz.append(float(words[2]))

magn = Magnetometer()

magn.calibrate(mx, my, mz)
print(magn.A_1)
print(magn.b)


