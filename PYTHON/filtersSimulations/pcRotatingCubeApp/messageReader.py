import math

import numpy as np

int16max = 32768
gyroSpectrum = 500
accSpectrum = 2
magnSpectrum = 8
from imu_filters.filters_helpers import normalize

def checkIfMessageIsCorrect(msg):
    if len(msg) < 26:
        return False

    # elif msg[0] != b'$':
    #     return False
    #
    # elif msg[25] != b'(':
    #     return False
    else:
        return True


def decodeMessage(msg, updateFunction):

    if checkIfMessageIsCorrect(msg):

        # gx = (np.int16(msg[1] + (msg[2] << 8)) - 450) / int16max * gyroSpectrum * math.pi / 180
        # gy = (np.int16(msg[3] + (msg[4] << 8)) + 760) / int16max * gyroSpectrum * math.pi / 180
        # gz = (np.int16(msg[5] + (msg[6] << 8)) + 224) / int16max * gyroSpectrum * math.pi / 180

        gx = (np.int16(msg[1] + (msg[2] << 8)) - 450) * 8.75e-3*(math.pi/180.0)
        gy = (np.int16(msg[3] + (msg[4] << 8)) + 760) * 8.75e-3*(math.pi/180.0)
        gz = (np.int16(msg[5] + (msg[6] << 8)) + 224) * 8.75e-3*(math.pi/180.0)

        ax = np.int16(msg[7] + (msg[8] << 8)) / int16max * accSpectrum
        ay = np.int16(msg[9] + (msg[10] << 8)) / int16max * accSpectrum
        az = np.int16(msg[11] + (msg[12] << 8)) / int16max * accSpectrum

        #ax, ay, az = normalize(ax, ay, az)
        #print(f'{ax},{ay},{az},     {gx},{gy},{gz}')

        mx = np.int16(msg[13] + (msg[14] << 8))# / int16max * magnSpectrum
        my = np.int16(msg[15] + (msg[16] << 8))# / int16max * magnSpectrum
        mz = np.int16(msg[17] + (msg[18] << 8))# / int16max * magnSpectrum

        A_1 = np.array([[0.000270, 0.000025, 0.000010],
                        [0.000025, 0.000299, -0.000006],
                        [0.000010, -0.000006, 0.000285]])

        b = np.array([-104.636886, -382.993375, -1188.400987]).reshape(3, 1)
        temp = [mx, my, mz]
        
        for i in range(0, len(temp)):
            temp[i] -= b[i]
        mx = float(A_1[0][0] * temp[0] + A_1[0][1] * temp[1] + A_1[0][2] * temp[2])
        my = float(A_1[1][0] * temp[0] + A_1[1][1] * temp[1] + A_1[1][2] * temp[2])
        mz = float(A_1[2][0] * temp[0] + A_1[2][1] * temp[1] + A_1[2][2] * temp[2])

        mx, my, mz = normalize(mx, my, mz)

        time = (msg[23] + (msg[24] << 8))

        updateFunction(gx, gy, gz, ax, ay, az, mx, my, mz, time)

        return True

    else:
        return False
