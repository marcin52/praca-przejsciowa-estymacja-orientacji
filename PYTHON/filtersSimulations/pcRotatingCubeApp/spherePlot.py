from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt

fig = plt.figure()
ax = plt.axes(projection='3d')

xdata = []
ydata = []
zdata = []

with open("magn.txt", "r") as f:
    data = f.readlines()

    for index, line in enumerate(data):

        if index == 0:
            continue

        words = line.split()
        xdata.append(float(words[0]))
        ydata.append(float(words[1]))
        zdata.append(float(words[2]))

ax.scatter3D(xdata, ydata, zdata, c=zdata, cmap='Greens')
plt.show()

#fig, axs = plt.subplots(1)
