import math

import numpy
import numpy as np
import scipy.linalg


def fit_ellipsoid(filename):

    with open(filename, "r") as f:

        data = f.readlines()

        size = len(data)

        D = np.zeros((10, size))

        for i, line in enumerate(data):

            if i == 0:
                continue

            words = line.split()

            x = float(words[0])
            y = float(words[1])
            z = float(words[2])

            D[0, i] = x * x
            D[1, i] = y * y
            D[2, i] = z * z
            D[3, i] = 2.0 * y * z
            D[4, i] = 2.0 * x * z
            D[5, i] = 2.0 * x * y
            D[6, i] = 2.0 * x
            D[7, i] = 2.0 * y
            D[8, i] = 2.0 * z
            D[9, i] = 1.0

        S = np.matmul(D, D.T)

        S11 = S[0:6, 0:6]
        S12 = S[0:6, 6:10]
        S12T = S[6:10, 0:6]
        S22 = S[6:10, 6:10]

        S22_inv = numpy.linalg.pinv(S22)

        S22a = np.matmul(S22_inv, S12T)
        SS = S11 - np.matmul(S12, S22a)

        # assuming k = 4
        C = np.array([[ -1.0, 1.0, 1.0, 0.0, 0.0, 0.0],
                    [1.0, -1.0, 1.0, 0.0,  0.0, 0.0],
                    [1.0, 1.0, -1.0,  0.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, -4.0, 0.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, -4.0, 0.0],
                    [0.0, 0.0, 0.0, 0.0, 0.0, -4.0]])

        C_inv = numpy.linalg.inv(C)

        E = np.matmul(C_inv, SS)

        vals, vectors = numpy.linalg.eig(E)

        index = 0
        maxval = vals[0]
        for i in range(0,6):

            if (vals[i] > maxval):
                maxval = vals[i]
                index = i

        v1 = vectors[:, index]

        if (v1[0] < 0.0):

            v1[0] = -v1[0]
            v1[1] = -v1[1]
            v1[2] = -v1[2]
            v1[3] = -v1[3]
            v1[4] = -v1[4]
            v1[5] = -v1[5]

        v2 = np.matmul(S22a, v1)

        v = np.zeros((10,1))

        v[0] = v1[0]
        v[1] = v1[1]
        v[2] = v1[2]
        v[3] = v1[3]
        v[4] = v1[4]
        v[5] = v1[5]
        v[6] = -v2[0]
        v[7] = -v2[1]
        v[8] = -v2[2]
        v[9] = -v2[3]

        # calculate bias B

        Q = np.zeros((3,3))

        Q[0, 0] = v[0]
        Q[0, 1] = v[5]
        Q[0, 2] = v[4]
        Q[1, 0] = v[5]
        Q[1, 1] = v[1]
        Q[1, 2] = v[3]
        Q[2, 0] = v[4]
        Q[2, 1] = v[3]
        Q[2, 2] = v[2]

        U = np.zeros((3,1))

        U[0] = v[6]
        U[1] = v[7]
        U[2] = v[8]

        Q_inv = numpy.linalg.inv(Q)

        B = -np.matmul(Q_inv, U)

        # calculate scale matrix A_-1

        QB = np.matmul(Q,B)
        BTQB = np.matmul(B.T, QB)

        J = v[9]
        HMB = math.sqrt(BTQB - J)

        SQ = scipy.linalg.sqrtm(Q)

        HM = 1.0

        A1 = np.zeros((3,3))
        for i in range(0,3):
            for j in range(0, 3):
                A1[i,j] = SQ[i,j] * HM / HMB

        return B, A1


if __name__=="__main__":
    B, A1 = fit_ellipsoid("mag1.txt")

    print(B)
    print(A1)