import numpy as np
import math
from numpy import array
from imu_filters.filters_helpers import normalize

def rollFromAcc(ax, ay, az):
    return math.atan2(ay, math.sqrt(ax ** 2 + az ** 2))


def pitchFromAcc(ax, ay, az):
    return math.atan2(ay, math.sqrt(ax ** 2 + az ** 2))


def yawFromAcc(ax, ay, az):
    return math.atan2(ay, ax)


def Rz(fi):
    matrix = array([[math.cos(fi), -math.sin(fi), 0],
                    [math.sin(fi), math.cos(fi), 0],
                    [0, 0, 1]])
    return matrix


def Ry(fi):
    matrix = array([[math.cos(fi), -math.sin(fi), 0],
                    [math.sin(fi), math.cos(fi), 0],
                    [0, 0, 1]])
    return matrix


def Rx(fi):
    matrix = array([[math.cos(fi), -math.sin(fi), 0],
                    [math.sin(fi), math.cos(fi), 0],
                    [0, 0, 1]])
    return matrix


def computeRotationMatrixFromAngles(roll, pitch, yaw):
    return Rz(roll) @ Ry(pitch) @ Rx(yaw)


def computeRotationMatrixFromQuaternions(q0, q1, q2, q3):

    matrix = 2 * array([[q0 ** 2 + q1 ** 2 - 1 / 2, q1 * q2 - q0 * q3, q1 * q3 + q0 * q2],
                        [q2 * q1 + q0 * q3, q0 ** 2 + q2 ** 2 - 1 / 2, q2 * q3 - q0 * q1],
                        [q3 * q1 - q0 * q2, q3 * q2 + q0 * q1, q0 ** 2 + q3 ** 2 - 1 / 2]])
    return matrix

def computeRotationMatrixFromQuaternions2(qx, qy, qz, qw):

    matrix = 2 * array([[1/2 - qy**2 - qz**2, qw*qz + qx*qy, qx*qz - qw*qy],
                        [qx*qy - qw*qz, 1/2 - qx**2 - qz**2, qw*qx + qy*qz],
                        [qw*qy + qx*qx, qy*qz - qw*qx, 1/2 - qx**2 - qy**2]])
    return matrix

def getInitialQuaternion(ax, ay, az, mx, my, mz):

    a = array([ax, ay, az])
    m = array([mx, my, mz])

    C1 = np.cross(np.cross(a, m), a)
    C2 = np.cross(a, m)
    C3 = a

    C = array([C1, C2, C3]).reshape([3,3]).T

    qw, qx, qy, qz = quaternionFromRotationMatrix(C)

    return normalize(qw, qx, qy, qz)

def eulerAnglesFromRotationMatrix(rotationMatrix):
    pitch = np.rad2deg(-math.asin(rotationMatrix[2][0]))
    roll = np.rad2deg(math.atan2(rotationMatrix[2][1], rotationMatrix[2][2]))
    yaw = np.rad2deg(math.atan2(rotationMatrix[1][0], rotationMatrix[0][0]))

    return roll, pitch, yaw


def quaternionFromRotationMatrix(rotationMatrix):
    q0 = math.sqrt(rotationMatrix[0][0] + rotationMatrix[1][1] + rotationMatrix[2][2] + 1) / 4
    q1 = (rotationMatrix[1][2] - rotationMatrix[2][1]) / 4 / q0
    q2 = (rotationMatrix[0][2] - rotationMatrix[2][0]) / 4 / q0
    q3 = (rotationMatrix[0][1] - rotationMatrix[1][0]) / 4 / q0

    return q0, q1, q2, q3


def anglesFromQuaternion(q0, q1, q2, q3, inDegrees=False):
    q0, q1, q2, q3 = normalize(q0, q1, q2, q3)

    roll = math.atan2(q0 * q1 + q2 * q3, 0.5 - q1 * q1 - q2 * q2)

    #val = -2.0 * (q1 * q3 - q0 * q2)
    val = 2.0 * (q0 * q2 - q1 * q3)

    if abs(val) <= 1:
        pitch = math.asin(val)
    else:
        pitch = 0

    yaw = math.atan2(q1 * q2 + q0 * q3, 0.5 - q2 * q2 - q3 * q3)

    yaw = -yaw + math.radians(14.9)
    if (yaw > math.radians(360.0)):
        yaw -= math.radians(360.0)
    if (yaw < 0):
        yaw += math.radians(360.0)

    if inDegrees:
        return math.degrees(roll), math.degrees(pitch), math.degrees(yaw)
    else:
        return roll, pitch, yaw

