import time

import matplotlib
import matplotlib.pyplot as plt
import sys
import numpy as np
from scipy import linalg

class Magnetometer(object):
    ''' Magnetometer class with calibration capabilities.

        Parameters
        ----------
        F : float (optional)
            Expected earth magnetic field intensity, default=1.
    '''

    def __init__(self, F=1.):

        # initialize values
        self.F   = F
        self.b   = np.zeros([3, 1])
        self.A_1 = np.eye(3)
        self.s = []
        self.samplesNumber = 0

    def read_precalibrated(self, mx, my, mz):

        A_1 = np.array([[0.48412466, 0.01769955, -0.01906895],
                         [0.01769955, 0.47992771, 0.019657],
                          [-0.01906895, 0.019657, 0.46725155]])

        b = np.array([0.25962979, 0.46833307, 1.00673323]).T
        s = np.array([mx, my, mz]).reshape(3, 1)
        s = np.dot(A_1, s - b)
        return [s[0, 0], s[1, 0], s[2, 0]]

    def read_precalibrated2(self, mx, my, mz):

        A_1 = np.array([[0.976241, 0.253233, 0.053246],
                         [0.253233, 1.135412, -0.056464],
                          [0.053246, -0.056464, 1.157710]])

        b = np.array([0.142692, 0.186746, -0.090884]).reshape(3, 1)
        s = np.array([mx, my, mz]).reshape(3, 1)
        s = np.dot(A_1, s - b)
        return [s[0, 0], s[1, 0], s[2, 0]]

    def read_precalibrated3(self, mx, my, mz):

        A_1 = np.array([[0.000270,  0.000025,  0.000010],
                          [  0.000025,  0.000299, -0.000006],
                          [ 0.000010, -0.000006,  0.000285]])

        b = np.array([-104.636886, -382.993375,   -1188.400987]).reshape(3, 1)
        s = np.array([mx, my, mz]).reshape(3, 1)
        s = np.dot(A_1, s - b)
        return [s[0, 0], s[1, 0], s[2, 0]]

    def read_precalibrated4(self, mx, my, mz):
        m_off = np.array([-826, -493, -1724])
        m_scl = np.array([3461, 3317, 3531])
        m_x = (mx - m_off[0]) / m_scl[0]
        m_y = (my - m_off[1]) / m_scl[1]
        m_z = (mz - m_off[2]) / m_scl[2]

        return [m_x, m_y, m_z]

    def read(self, mx, my, mz):
        ''' Get a sample.

            Returns
            -------
            s : list
                The sample in uT, [x,y,z] (corrected if performed calibration).
        '''
        s = np.array([mx, my, mz]).reshape(3, 1)
        s = np.dot(self.A_1, s - self.b)
        return [s[0,0], s[1,0], s[2,0]]

    def calibrate(self, mx, my, mz):
        ''' Performs calibration. '''

        while self.samplesNumber < len(mx):
            self.s.append([mx, my, mz])
            self.samplesNumber += 1
            sys.stdout.write('\rTotal: %d' % self.samplesNumber)
            sys.stdout.flush()

        # ellipsoid fit
        s = np.array(self.s).T
        M, n, d = self.__ellipsoid_fit(s)

        # calibration parameters
        # note: some implementations of sqrtm return complex type, taking real
        M_1 = linalg.inv(M)
        self.b = -np.dot(M_1, n)
        self.A_1 = np.real(self.F / np.sqrt(np.dot(n.T, np.dot(M_1, n)) - d) *
                           linalg.sqrtm(M))

        xdata = []
        ydata = []
        zdata = []
        f = open("./magn.txt", "w")
        for i in range(0, len(s[0])):

            mx, my, mz = self.read(s[0][i], s[1][i], s[2][i])

            f.write( f"{mx:9.4f} {my:9.4f} {mz:9.4f} \n")
            # zdata.append(mz)
            # xdata.append(mx)
            # ydata.append(my)



        f.close()
        # plt.figure(1)
        # first_figure = matplotlib.figure.Figure()
        # first_figure_axis = first_figure.add_subplot()
        # first_figure_axis.plot(xdata, ydata)
        # #ax = plt.axes(projection='3d')
        # #ax.scatter3D(xdata, ydata, zdata, cmap='Greens')
        # plt.show()





    def __ellipsoid_fit(self, s):
        ''' Estimate ellipsoid parameters from a set of points.

            Parameters
            ----------
            s : array_like
              The samples (M,N) where M=3 (x,y,z) and N=number of samples.

            Returns
            -------
            M, n, d : array_like, array_like, float
              The ellipsoid parameters M, n, d.

            References
            ----------
            .. [1] Qingde Li; Griffiths, J.G., "Least squares ellipsoid specific
               fitting," in Geometric Modeling and Processing, 2004.
               Proceedings, vol., no., pp.335-340, 2004
        '''

        # D (samples)
        D = np.array([s[0]**2., s[1]**2., s[2]**2.,
                      2.*s[1]*s[2], 2.*s[0]*s[2], 2.*s[0]*s[1],
                      2.*s[0], 2.*s[1], 2.*s[2], np.ones_like(s[0])])

        # S, S_11, S_12, S_21, S_22 (eq. 11)
        S = np.dot(D, D.T)
        S_11 = S[:6,:6]
        S_12 = S[:6,6:]
        S_21 = S[6:,:6]
        S_22 = S[6:,6:]

        # C (Eq. 8, k=4)
        C = np.array([[-1,  1,  1,  0,  0,  0],
                      [ 1, -1,  1,  0,  0,  0],
                      [ 1,  1, -1,  0,  0,  0],
                      [ 0,  0,  0, -4,  0,  0],
                      [ 0,  0,  0,  0, -4,  0],
                      [ 0,  0,  0,  0,  0, -4]])

        # v_1 (eq. 15, solution)
        E = np.dot(linalg.inv(C),
                   S_11 - np.dot(S_12, np.dot(linalg.inv(S_22), S_21)))

        E_w, E_v = np.linalg.eig(E)

        v_1 = E_v[:, np.argmax(E_w)]
        if v_1[0] < 0: v_1 = -v_1

        # v_2 (eq. 13, solution)
        v_2 = np.dot(np.dot(-np.linalg.inv(S_22), S_21), v_1)

        # quadric-form parameters
        M = np.array([[v_1[0], v_1[3], v_1[4]],
                      [v_1[3], v_1[1], v_1[5]],
                      [v_1[4], v_1[5], v_1[2]]])
        n = np.array([[v_2[0]],
                      [v_2[1]],
                      [v_2[2]]])
        d = v_2[3]

        return M, n, d