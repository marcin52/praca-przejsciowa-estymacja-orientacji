import math
import numpy
from filterpy.common import Q_discrete_white_noise
from filterpy.kalman import KalmanFilter
from numpy import array
import numpy as num
import matplotlib.pyplot as plt
import random

def generateValues(size, trueValue, delta):

    array = num.ndarray(size, float)

    for i in range(0, size):
        #array[i] = random.uniform(trueValue - delta, trueValue + delta)
        array[i] = trueValue + numpy.random.randn()*delta

    return array

def generateVelocities(size, positions, delta, timeSample):

    array = num.ndarray(size, float)

    for i in range(1, size):
        #array[i] = (positions[i] - positions[i - 1]) / timeSample + random.uniform(-delta,  delta)
        array[i] = (positions[i] - positions[i - 1]) / timeSample + numpy.random.randn()*delta

    return array

def generateZeroVelocities(size, delta):

    array = num.ndarray(size, float)

    for i in range(1, size):
        array[i] = numpy.random.randn()*delta

    return array

def generateStaticData(size, true_ax, true_ay, true_az, sigma_gyro, sigma_acc):
    gx = generateZeroVelocities(size, sigma_gyro)
    gy = generateZeroVelocities(size, sigma_gyro)
    gz = generateZeroVelocities(size, sigma_gyro)
    ax = generateValues(size, true_ax, sigma_acc)
    ay = generateValues(size, true_ay, sigma_acc)
    az = generateValues(size, true_az, sigma_acc)

    return gx, gy, gz, ax, ay, az

def addGyroBias(gx, gy, gz, gyro_bias, simulation_size):

    for i in range(0, simulation_size):
        gx[i] += random.uniform(-gyro_bias, gyro_bias) * i
        gy[i] += random.uniform(-gyro_bias, gyro_bias) * i
        gz[i] += random.uniform(-gyro_bias, gyro_bias) * i

def simulateMARG(simulation_size, update_func, gx, gy, gz, ax, ay, az, mx, my, mz):

    roll_vec = num.ndarray(simulation_size, float)
    pitch_vec = num.ndarray(simulation_size, float)
    yaw_vec = num.ndarray(simulation_size, float)

    for i in range(0, simulation_size):

        roll, pitch, yaw = update_func(gx[i], gy[i], gz[i], ax[i], ay[i], az[i], mx[i], my[i], mz[i])

        roll_vec[i] = roll
        pitch_vec[i] = pitch
        yaw_vec[i] = yaw

    return roll_vec, pitch_vec, yaw_vec

def simulateIMU(simulation_size, update_func, gx, gy, gz, ax, ay, az):

    roll_vec = num.ndarray(simulation_size, float)
    pitch_vec = num.ndarray(simulation_size, float)
    yaw_vec = num.ndarray(simulation_size, float)

    for i in range(0, simulation_size):

        roll, pitch, yaw = update_func(gx[i], gy[i], gz[i], ax[i], ay[i], az[i])

        roll_vec[i] = roll
        pitch_vec[i] = pitch
        yaw_vec[i] = yaw

    return roll_vec, pitch_vec, yaw_vec

def readDataFile(filename):
    roll = []
    pitch = []
    yaw = []
    gx = []
    gy = []
    gz = []
    ax = []
    ay = []
    az = []
    mx = []
    my = []
    mz = []

    with open(filename, "r") as f:

        data = f.readlines()

        for index, line in enumerate(data):

            if index == 0:
                continue

            words = line.split()

            roll.append(float(words[0]))
            pitch.append(float(words[1]))
            yaw.append(float(words[2]))
            gx.append(float(words[3]))
            gy.append(float(words[4]))
            gz.append(float(words[5]))
            ax.append(float(words[6]))
            ay.append(float(words[7]))
            az.append(float(words[8]))
            mx.append(float(words[9]))
            my.append(float(words[10]))
            mz.append(float(words[11]))

    return roll, pitch, yaw, gx, gy, gz, ax, ay, az, mx, my, mz


def readRealSensorDataFile(filename):

    time = []
    gx = []
    gy = []
    gz = []
    ax = []
    ay = []
    az = []

    with open(filename, "r") as f:

        data = f.readlines()

        for index, line in enumerate(data):

            if index == 0:
                continue

            words = line.split()

            time.append(float(words[0]) / 1000 ) # in seconds
            gx.append(float(words[1]) * 500.0 / 32768 * math.pi / 180)
            gy.append(float(words[2]) * 500.0 / 32768 * math.pi / 180)
            gz.append(float(words[3]) * 500.0 / 32768 * math.pi / 180)
            ax.append(float(words[4]) * 2.0 / 32768 )
            ay.append(float(words[5]) * 2.0 / 32768 )
            az.append(float(words[6])* 2.0 / 32768 )

    return time, gx, gy, gz, ax, ay, az


def calculateError(filtered, real):

    if len(filtered) != len(real):
        return
    else:

        error = 0
        for i in range(0, len(real)):

            if filtered[i] - real[i] > 300:

                error += (filtered[i] - real[i] - 360) ** 2
            elif filtered[i] - real[i] < - 300:
                error += (filtered[i] - real[i] + 360) ** 2
            else:
                error += (filtered[i] - real[i]) ** 2

        print(error / len(real))
