import math

import matplotlib.pyplot as plt
from imu_filters import mahony, madgwick, dspLib
from helpers import rotation, simulation
import eulerUKF

from imu_filters.quaternionUKF import QuaternionUKF
from imu_filters.dcmKF import DCM_KF

def eulerUKF_Update(gx, gy, gz, ax, ay, az):

    #lowPassedAx = accxButterFilter.filterStep(ax)
    #lowPassedAy = accxButterFilter.filterStep(ay)
    #lowPassedAz = accxButterFilter.filterStep(az)

    euler.predict(wx = gx, wy = gy, wz = gz )
    euler.update( z = (ax, ay, az) )

    roll = euler.x[0]
    pitch = euler.x[1]
    yaw = 0

    return math.degrees(roll), math.degrees(pitch), math.degrees(yaw)

if __name__ == '__main__':

    roll, pitch, yaw, gx, gy, gz, ax, ay, az, mx, my, mz = simulation.readDataFile("testing_data/noisy_data1.txt")

    simulationSize = len(ax)
    timeSample = 0.01
    magn_sigma = 5
    gyro_sigma = math.radians(80)
    gyro_bias = math.radians(0.3)
    acc_sigma = 0.2

    accxFilter = dspLib.SimpleLowPassFilter(ax[0], 0.66)
    accyFilter = dspLib.SimpleLowPassFilter(ay[0], 0.66)
    acczFilter = dspLib.SimpleLowPassFilter(az[0], 0.66)
    gxFilter = dspLib.SimpleHighPassFilter(gx[0], 0.001)
    gyFilter = dspLib.SimpleHighPassFilter(gy[0], 0.1)
    gzFilter = dspLib.SimpleHighPassFilter(gz[0], 0.1)

    for i in range(1, len(ax)):
        ax[i] = accxFilter.filterStep(ax[i])
        ay[i] = accyFilter.filterStep(ay[i])
        az[i] = acczFilter.filterStep(az[i])
    #     gx[i] = gxFilter.filterStep(gx[i])
    #     gy[i] = gyFilter.filterStep(gy[i])
    #     gz[i] = gzFilter.filterStep(gz[i])

    mahonyFilter = mahony.MahonyFilter(timeSample, 12, 0.1, 0, 0, 0)
    madgwickFilter = madgwick.MadgwickFilter(timeSample, math.sqrt(3 / 4) * 1.5, 0, 0, 0,
                                             math.sqrt(3 / 4) * gyro_bias)

    quternionUKF = QuaternionUKF(timeSample, gyro_sigma, acc_sigma, estimateBias=True)
    dcmlin = DCM_KF(timeSample, math.radians(75), 0.23)
    euler = eulerUKF.initUKF(timeSample, acc_sigma, magn_sigma, gyro_sigma)

    eul_roll, eul_pitch, eul_yaw = simulation.simulateIMU(simulationSize, eulerUKF_Update, gx, gy, gz, ax, ay, az)
    dcm_roll, dcm_pitch, dcm_yaw = simulation.simulateIMU(simulationSize, dcmlin.updateIMU, gx, gy, gz, ax, ay, az)
    qukf_roll, qukf_pitch, qukf_yaw = simulation.simulateIMU(simulationSize, quternionUKF.updateIMU, gx, gy, gz, ax, ay, az)
    mah_roll, mah_pitch, mah_yaw = simulation.simulateIMU(simulationSize, mahonyFilter.simulationIMU_update, gx, gy, gz, ax, ay, az)
    mad_roll, mad_pitch, mad_yaw = simulation.simulateIMU(simulationSize, madgwickFilter.simulationIMU_update, gx, gy, gz, ax, ay, az)

    print("roll")
    simulation.calculateError(dcm_roll, roll)
    simulation.calculateError(qukf_roll, roll)
    simulation.calculateError(mah_roll, roll)
    simulation.calculateError(mad_roll, roll)

    print("pitch")
    simulation.calculateError(dcm_pitch, pitch)
    simulation.calculateError(qukf_pitch, pitch)
    simulation.calculateError(mah_pitch, pitch)
    simulation.calculateError(mad_pitch, pitch)

    x = range(0, simulationSize)
    fig, axs = plt.subplots(6, 3)

    axs[0, 0].plot(x, pitch)
    axs[0, 0].set_title('pitch')
    axs[0, 1].plot(x, roll)
    axs[0, 1].set_title('roll')
    axs[0, 2].plot(x, yaw)
    axs[0, 2].set_title('yaw')

    axs[1, 0].plot(x, mah_pitch -  pitch)
    axs[1, 0].set_title('mahony pitch')
    axs[1, 1].plot(x, mah_roll - roll)
    axs[1, 1].set_title('mahony roll')
    axs[1, 2].plot(x, mah_yaw - yaw)
    axs[1, 2].set_title('mahony yaw')

    axs[2, 0].plot(x, mad_pitch - pitch)
    axs[2, 0].set_title('madgwick pitch')
    axs[2, 1].plot(x, mad_roll - roll)
    axs[2, 1].set_title('madgwick roll')
    axs[2, 2].plot(x, mad_yaw - yaw)
    axs[2, 2].set_title('madgwick yaw')

    axs[3, 0].plot(x, dcm_pitch - pitch)
    axs[3, 0].set_title('dcm kalman pitch')
    axs[3, 1].plot(x, dcm_roll - roll)
    axs[3, 1].set_title('dcm kalman roll')
    axs[3, 2].plot(x, dcm_yaw - yaw)
    axs[3, 2].set_title('dcm kalman yaw')

    axs[4, 0].plot(x, qukf_pitch - pitch)
    axs[4, 0].set_title('quaternion ukf pitch')
    axs[4, 1].plot(x, qukf_roll - roll)
    axs[4, 1].set_title('quaternion ukf roll')
    axs[4, 2].plot(x, qukf_yaw - yaw)
    axs[4, 2].set_title('quaternion ukf yaw')

    axs[5, 0].plot(x, eul_pitch - pitch)
    axs[5, 0].set_title('euler ukf pitch')
    axs[5, 1].plot(x, eul_roll - roll)
    axs[5, 1].set_title('euler ukf roll')
    axs[5, 2].plot(x, qukf_yaw - yaw)
    axs[5, 2].set_title('euler ukf yaw')

    plt.setp(axs, xticks=[])

    plt.legend(loc="upper left")


    #plt.tight_layout()
    plt.show()