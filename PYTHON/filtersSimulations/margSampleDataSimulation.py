import math

import matplotlib.pyplot as plt
from imu_filters import mahony, madgwick, dspLib
from helpers import rotation, simulation
import eulerUKF

from imu_filters.quaternionUKF import QuaternionUKF
from imu_filters.dcmKF import DCM_KF

def eulerUKF_Update(gx, gy, gz, ax, ay, az):

    euler.predict(wx = gx, wy = gy, wz = gz )
    euler.update( z = (ax, ay) )

    roll = euler.x[0]
    pitch = euler.x[1]
    yaw = 0

    return math.degrees(roll), math.degrees(pitch), math.degrees(yaw)

if __name__ == '__main__':

    roll, pitch, yaw, gx, gy, gz, ax, ay, az, mx, my, mz = simulation.readDataFile("testing_data/only_z_axis_sim_data.txt")

    simulationSize = len(ax)
    timeSample = 0.01
    magn_sigma = 5
    gyro_sigma = math.radians(10)
    gyro_bias = math.radians(0.3)
    acc_sigma = 0.02

    mahonyFilter = mahony.MahonyFilter(timeSample, 0.15, 1, 0, 0, 0)
    madgwickFilter = madgwick.MadgwickFilter(timeSample, math.sqrt(3 / 4) * 2, 0, 0, 0,
                                             math.sqrt(3 / 4) * gyro_bias)

    quternionUKF = QuaternionUKF(timeSample, gyro_sigma, acc_sigma, estimateBias=True, MARG_filt=True)
    dcmlin = DCM_KF(timeSample, gyro_sigma, acc_sigma)
    euler = eulerUKF.initUKF(timeSample, acc_sigma, magn_sigma, gyro_sigma)

    eul_roll, eul_pitch, eul_yaw = simulation.simulateIMU(simulationSize, eulerUKF_Update, gx, gy, gz, ax, ay, az)
    dcm_roll, dcm_pitch, dcm_yaw = simulation.simulateIMU(simulationSize, dcmlin.updateIMU, gx, gy, gz, ax, ay, az)
    qukf_roll, qukf_pitch, qukf_yaw = simulation.simulateMARG(simulationSize, quternionUKF.updateMARG, gx, gy, gz, ax, ay, az, mx, my, mz)
    mah_roll, mah_pitch, mah_yaw = simulation.simulateMARG(simulationSize, mahonyFilter.simulationMARG_update, gx, gy, gz, ax, ay, az, mx, my, mz)
    mad_roll, mad_pitch, mad_yaw = simulation.simulateMARG(simulationSize, madgwickFilter.simulationMARG_update, gx, gy, gz, ax, ay, az, mx, my, mz)

    x = range(0, simulationSize)
    fig, axs = plt.subplots(5, 3)

    axs[0, 0].plot(x, pitch)
    axs[0, 0].set_title('pitch')
    axs[0, 1].plot(x, roll)
    axs[0, 1].set_title('roll')
    axs[0, 2].plot(x, yaw)
    axs[0, 2].set_title('yaw')

    axs[1, 0].plot(x, mah_pitch)
    axs[1, 0].set_title('mahony pitch')
    axs[1, 1].plot(x, mah_roll)
    axs[1, 1].set_title('mahony roll')
    axs[1, 2].plot(x, mah_yaw)
    axs[1, 2].set_title('mahony yaw')

    axs[2, 0].plot(x, mad_pitch)
    axs[2, 0].set_title('madgwick pitch')
    axs[2, 1].plot(x, mad_roll)
    axs[2, 1].set_title('madgwick roll')
    axs[2, 2].plot(x, mad_yaw)
    axs[2, 2].set_title('madgwick yaw')

    axs[3, 0].plot(x, dcm_pitch)
    axs[3, 0].set_title('dcm kalman pitch')
    axs[3, 1].plot(x, dcm_roll)
    axs[3, 1].set_title('dcm kalman roll')
    axs[3, 2].plot(x, dcm_yaw)
    axs[3, 2].set_title('dcm kalman yaw')

    axs[4, 0].plot(x, qukf_pitch)
    axs[4, 0].set_title('quaternion ukf pitch')
    axs[4, 1].plot(x, qukf_roll)
    axs[4, 1].set_title('quaternion ukf roll')
    axs[4, 2].plot(x, qukf_yaw)
    axs[4, 2].set_title('quaternion ukf yaw')

    plt.setp(axs, xticks=[])

    plt.legend(loc="upper left")


    #plt.tight_layout()
    plt.show()