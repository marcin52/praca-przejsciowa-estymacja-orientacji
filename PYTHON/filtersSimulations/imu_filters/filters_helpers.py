import numpy as np
import math


def normalize(*args):

    norm = 0
    result = list()

    for x in args:
        norm += x*x

    if norm>0:
        norm = math.sqrt(norm)

    if norm != 0:

        for x in args:
            result.append(x / norm)
    else:
        for x in args:
            result.append(x)

    return result

def integrate(dt, multiplier,  *args):

    result = list()

    for x in args:
        result.append(multiplier * x * dt)

    return result

def degToRad(*args):

    result = list()

    for x in args:
        result.append( np.deg2rad(x) )

    return result

def radToDeg(*args):
    result = list()

    for x in args:
        result.append(np.rad2deg(x))

    return result

def vectorCross( a, b):
    res1 = (a[1] * b[2] - a[2] * b[1])
    res2 = (a[2] * b[0] - a[0] * b[2])
    res3 = (a[0] * b[1] - a[1] * b[0])

    return res1, res2, res3
