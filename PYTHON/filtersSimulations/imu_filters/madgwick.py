import numpy as np
from helpers import rotation
import math
from imu_filters.filters_helpers import normalize
from imu_filters.filters_helpers import degToRad


class MadgwickFilter():

    def __init__(self, dt, beta, roll=0, pitch=0, yaw=0, zeta = 0):
        self.dt = dt
        self.halfT = dt / 2
        self.beta = beta
        self.zeta = zeta

        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

        self.rotationMatrix = rotation.computeRotationMatrixFromAngles(roll, pitch, yaw)

        self.exInt = 0
        self.eyInt = 0
        self.ezInt = 0

        self.q0 = 1
        self.q1 = 0
        self.q2 = 0
        self.q3 = 0

        self.b_x = 0
        self.b_z = 0
        self.w_bx = 0
        self.w_by = 0
        self.w_bz = 0

    def updateIMU(self, gx, gy, gz, ax, ay, az, computeGyroBiasError=False):

        # Convert gyroscope degrees/sec to radians/sec
        gx, gy, gz = degToRad(gx, gy, gz)

        qDot1, qDot2, qDot3, qDot4 = self.calcQuaternionRateOfChange(gx, gy, gz)

        # Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if( not ((ax == 0.0) and (ay == 0.0) and (az == 0.0))):

           # Normalise accelerometer measurement
            ax, ay, az = normalize(ax, ay, az)

            s0, s1, s2, s3 = self.IMU_gradientDescent(ax, ay, az)

            if computeGyroBiasError:
                self.handleGyroBias(gx, gy, gz, s0, s1, s2, s3)

           # Apply feedback step
            qDot1 -= self.beta * s0
            qDot2 -= self.beta * s1
            qDot3 -= self.beta * s2
            qDot4 -= self.beta * s3

        self.integrateQuaternion(qDot1, qDot2, qDot3, qDot4)

        # calculate rotation matrix
        self.rotationMatrix = rotation.computeRotationMatrixFromQuaternions(self.q0, self.q1, self.q2, self.q3)

        # calculate euler angle from quaternion
        self.roll, self.pitch, self.yaw = rotation.anglesFromQuaternion(self.q0, self.q1, self.q2, self.q3, inDegrees=True)

    def calcQuaternionRateOfChange(self, gx, gy, gz):
        qDot1 = 0.5 * (-self.q1 * gx - self.q2 * gy - self.q3 * gz)
        qDot2 = 0.5 * (self.q0 * gx + self.q2 * gz - self.q3 * gy)
        qDot3 = 0.5 * (self.q0 * gy - self.q1 * gz + self.q3 * gx)
        qDot4 = 0.5 * (self.q0 * gz + self.q1 * gy - self.q2 * gx)

        return qDot1, qDot2, qDot3, qDot4

    def integrateQuaternion(self, qDot1, qDot2, qDot3, qDot4):
        # Integrate rate of change of quaternion to yield quaternion
        self.q0 += qDot1 * self.dt
        self.q1 += qDot2 * self.dt
        self.q2 += qDot3 * self.dt
        self.q3 += qDot4 * self.dt

        # Normalise quaternion
        self.q0, self.q1, self.q2, self.q3 = normalize(self.q0, self.q1, self.q2, self.q3)

    def handleGyroBias(self, gx, gy, gz, s0, s1, s2, s3):

        # compute angular estimated direction of the gyroscope error
        w_err_x = self.q0 * s1 - self.q1 * s0 - self.q2 * s3 + self.q3 * s2
        w_err_y = self.q0 * s2 + self.q1 * s3 - self.q2 * s0 - self.q3 * s1
        w_err_z = self.q0 * s3 - self.q1 * s2 + self.q2 * s1 - self.q3 * s0

        # compute and remove the gyroscope baises
        self.w_bx += w_err_x * self.dt * self.zeta
        self.w_by += w_err_y * self.dt * self.zeta
        self.w_bz += w_err_z * self.dt * self.zeta
        res_gx = gx - self.w_bx
        res_gy = gy - self.w_by
        res_gz = gz - self.w_bz

        return res_gx, res_gy, res_gz

    def IMU_gradientDescent(self, ax, ay, az):
        # Auxiliary variables to avoid repeated arithmetic
        _2q0 = 2.0 * self.q0
        _2q1 = 2.0 * self.q1
        _2q2 = 2.0 * self.q2
        _2q3 = 2.0 * self.q3
        _4q0 = 4.0 * self.q0
        _4q1 = 4.0 * self.q1
        _4q2 = 4.0 * self.q2
        _8q1 = 8.0 * self.q1
        _8q2 = 8.0 * self.q2
        q0q0 = self.q0 * self.q0
        q1q1 = self.q1 * self.q1
        q2q2 = self.q2 * self.q2
        q3q3 = self.q3 * self.q3

        # Gradient decent algorithm corrective step
        s0 = _4q0 * q2q2 + _2q2 * ax + _4q0 * q1q1 - _2q1 * ay
        s1 = _4q1 * q3q3 - _2q3 * ax + 4.0 * q0q0 * self.q1 - _2q0 * ay - _4q1 + _8q1 * q1q1 + _8q1 * q2q2 + _4q1 * az
        s2 = 4.0 * q0q0 * self.q2 + _2q0 * ax + _4q2 * q3q3 - _2q3 * ay - _4q2 + _8q2 * q1q1 + _8q2 * q2q2 + _4q2 * az
        s3 = 4.0 * q1q1 * self.q3 - _2q1 * ax + 4.0 * q2q2 * self.q3 - _2q2 * ay

        return normalize(s0, s1, s2, s3)

    def updateMARG(self, gx, gy, gz, ax, ay, az, mx, my, mz):

        # Use IMU algorithm if magnetometer measurement invalid(avoids NaN in magnetometer normalisation)
        if (mx == 0.0) and (my == 0.0) and (mz == 0.0):
           
            self.updateIMU(gx, gy, gz, ax, ay, az)
            return

        # Convert gyroscope degrees/sec to radians/sec
        gx = np.deg2rad(gx)
        gy = np.deg2rad(gy)
        gz = np.deg2rad(gz)

        # Rate of change of quaternion from gyroscope
        qDot1 = 0.5 * (-self.q1 * gx - self.q2 * gy - self.q3 * gz)
        qDot2 = 0.5 * (self.q0 * gx + self.q2 * gz - self.q3 * gy)
        qDot3 = 0.5 * (self.q0 * gy - self.q1 * gz + self.q3 * gx)
        qDot4 = 0.5 * (self.q0 * gz + self.q1 * gy - self.q2 * gx)

        # Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if (not ((ax == 0.0) and (ay == 0.0) and (az == 0.0))):

            # Normalise accelerometer measurement
            recipNorm = 1 / math.sqrt(ax * ax + ay * ay + az * az)
            ax *= recipNorm
            ay *= recipNorm
            az *= recipNorm

            # Normalise magnetometer measurement
            recipNorm = 1 / math.sqrt(mx * mx + my * my + mz * mz)
            mx *= recipNorm
            my *= recipNorm
            mz *= recipNorm

            # Auxiliary variables to avoid repeated arithmetic
            _2q0mx = 2.0 *self.q0 * mx
            _2q0my = 2.0 *self.q0 * my
            _2q0mz = 2.0*self.q0 * mz
            _2q1mx = 2.0 *self.q1 * mx
            _2q0 = 2.0 *self.q0
            _2q1 = 2.0 *self.q1
            _2q2 = 2.0 *self.q2
            _2q3 = 2.0 *self.q3
            _2q0q2 = 2.0 *self.q0 *self.q2
            _2q2q3 = 2.0 *self.q2 *self.q3
            q0q0 =self.q0 *self.q0 
            q0q1 =self.q0 *self.q1 
            q0q2 =self.q0 *self.q2 
            q0q3 =self.q0 *self.q3 
            q1q1 =self.q1 *self.q1 
            q1q2 =self.q1 *self.q2 
            q1q3 =self.q1 *self.q3 
            q2q2 =self.q2 *self.q2 
            q2q3 =self.q2 *self.q3 
            q3q3 =self.q3 *self.q3 

            # Reference direction of Earth's magnetic field
            hx = mx * q0q0 - _2q0my * self.q3 + _2q0mz * self.q2  + mx * q1q1 + _2q1 * my * self.q2 + _2q1 * mz * self.q3 - mx * q2q2 - mx * q3q3
            hy = _2q0mx * self.q3 + my * q0q0 - _2q0mz * self.q1  + _2q1mx * self.q2 - my * q1q1 + my * q2q2 + _2q2 * mz * self.q3 - my * q3q3
            _2bx = math.sqrt(hx * hx + hy * hy) 
            _2bz = -_2q0mx * self.q2 + _2q0my * self.q1 + mz * q0q0 + _2q1mx * self.q3 - mz * q1q1 + _2q2 * my * self.q3 - mz * q2q2 + mz * q3q3
            _4bx = 2.0* _2bx 
            _4bz = 2.0 * _2bz

            # Gradient decent algorithm corrective step
            s0 = -_2q2 * (2.0 * q1q3 - _2q0q2 - ax) + _2q1 * (2.0 * q0q1 + _2q2q3 - ay) - _2bz * self.q2 * (
                        _2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * self.q3 + _2bz * self.q1) * (
                             _2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * self.q2 * (
                             _2bx * (q0q2 + q1q3) + _2bz * (0.5 - q1q1 - q2q2) - mz) 
            s1 = _2q3 * (2.0 * q1q3 - _2q0q2 - ax) + _2q0 * (2.0 * q0q1 + _2q2q3 - ay) - 4.0 * self.q1 * (1 - 2.0 * q1q1 - 2.0 * q2q2 - az) + _2bz * self.q3 * (
                        _2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * self.q2 + _2bz * self.q0) * (
                        _2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * self.q3 - _4bz * self.q1) * (
                        _2bx * (q0q2 + q1q3) + _2bz * (0.5 - q1q1 - q2q2) - mz)
            s2 = -_2q0 * (2.0 * q1q3 - _2q0q2 - ax) + _2q3 * (2.0 * q0q1 + _2q2q3 - ay) - 4.0 * self.q2 * (1 - 2.0 * q1q1 - 2.0 * q2q2 - az) + (-_4bx * self.q2 - _2bz * self.q0) * (
                        _2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (_2bx * self.q1 + _2bz * self.q3) * (
                        _2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + (_2bx * self.q0 - _4bz * self.q2) * (
                        _2bx * (q0q2 + q1q3) + _2bz * (0.5 - q1q1 - q2q2) - mz)
            s3 = _2q1 * (2.0 * q1q3 - _2q0q2 - ax) + _2q2 * (2.0 * q0q1 + _2q2q3 - ay) + (-_4bx * self.q3 + _2bz * self.q1) * (
                        _2bx * (0.5 - q2q2 - q3q3) + _2bz * (q1q3 - q0q2) - mx) + (-_2bx * self.q0 + _2bz * self.q2) * (
                             _2bx * (q1q2 - q0q3) + _2bz * (q0q1 + q2q3) - my) + _2bx * self.q1 * (
                             _2bx * (q0q2 + q1q3) + _2bz * (0.5 - q1q1 - q2q2) - mz) 
            recipNorm = 1 / math.sqrt(s0 * s0 + s1 * s1 + s2 * s2 + s3 * s3)  # normalise step magnitude
            s0 *= recipNorm 
            s1 *= recipNorm 
            s2 *= recipNorm 
            s3 *= recipNorm 

            # Apply feedback step
            qDot1 -= self.beta * s0
            qDot2 -= self.beta * s1
            qDot3 -= self.beta * s2
            qDot4 -= self.beta * s3

        # Integrate rate of change of quaternion to yield quaternion
        self.q0 += qDot1 * self.dt
        self.q1 += qDot2 * self.dt
        self.q2 += qDot3 * self.dt
        self.q3 += qDot4 * self.dt

        # Normalise quaternion
        recipNorm = 1 / math.sqrt(self.q0 * self.q0 +self.q1 *self.q1 +self.q2 *self.q2 +self.q3 *self.q3)
        self.q0 *= recipNorm
        self.q1 *= recipNorm
        self.q2 *= recipNorm
        self.q3 *= recipNorm

        # calculate euler angle from quaternion
        self.roll, self.pitch, self.yaw = rotation.anglesFromQuaternion(self.q0, self.q1, self.q2, self.q3,
                                                                        inDegrees=True)

    def updateMARG_withBiasAndMagneticFieldCorrection(self, g_x, g_y, g_z, a_x, a_y, a_z, m_x, m_y, m_z):

        # Use IMU algorithm if magnetometer measurement invalid(avoids NaN in magnetometer normalisation)
        if (m_x == 0.0) and (m_y == 0.0) and (m_z == 0.0):
            self.updateIMU(g_x, g_y, g_z, a_x, a_y, a_z)
            return

        # Convert gyroscope degrees/sec to radians/sec
        g_x = np.deg2rad(g_x)
        g_y = np.deg2rad(g_y)
        g_z = np.deg2rad(g_z)

        # axulirary variables to avoid reapeated calcualtions
        halfSEq_1 = 0.5  * self.q0
        halfSEq_2 = 0.5  * self.q1
        halfSEq_3 = 0.5  * self.q2
        halfSEq_4 = 0.5  * self.q3
        twoSEq_1 = 2.0  * self.q0
        twoSEq_2 = 2.0  * self.q1
        twoSEq_3 = 2.0  * self.q2
        twoSEq_4 = 2.0  * self.q3
        twob_x = 2.0 * self.b_x
        twob_z = 2.0 * self.b_z
        twob_xSEq_1 = 2.0 * self.b_x * self.q0
        twob_xSEq_2 = 2.0 * self.b_x * self.q1
        twob_xSEq_3 = 2.0 * self.b_x * self.q2
        twob_xSEq_4 = 2.0 * self.b_x * self.q3
        twob_zSEq_1 = 2.0 * self.b_z * self.q0
        twob_zSEq_2 = 2.0 * self.b_z * self.q1
        twob_zSEq_3 = 2.0 * self.b_z * self.q2
        twob_zSEq_4 = 2.0 * self.b_z * self.q3
        SEq_1SEq_3 = self.q0 * self.q2
        SEq_2SEq_4 = self.q1 * self.q3
        twom_x = 2.0 * m_x
        twom_y = 2.0 * m_y
        twom_z = 2.0 * m_z

        # normalise the accelerometermeasurement
        norm = math.sqrt(a_x * a_x + a_y * a_y + a_z * a_z)
        a_x /= norm
        a_y /= norm
        a_z /= norm

        # normalise the magnetometer measurement
        norm = math.sqrt(m_x * m_x + m_y * m_y + m_z * m_z)
        m_x /= norm
        m_y /= norm
        m_z /= norm

        # compute the objective function and Jacobian
        f_1 = twoSEq_2 * self.q3 - twoSEq_1 * self.q2 - a_x
        f_2 = twoSEq_1 * self.q1 + twoSEq_3 * self.q3 - a_y
        f_3 = 1.0 - twoSEq_2 * self.q1 - twoSEq_3 * self.q2 - a_z
        f_4 = twob_x * (0.5 - self.q2 * self.q2 - self.q3 * self.q3) + twob_z * (SEq_2SEq_4 - SEq_1SEq_3) - m_x
        f_5 = twob_x * (self.q1 * self.q2 - self.q0 * self.q3) + twob_z * (self.q0 * self.q1 + self.q2 * self.q3) - m_y
        f_6 = twob_x * (SEq_1SEq_3 + SEq_2SEq_4) + twob_z * (0.5 - self.q1 * self.q1 - self.q2 * self.q2) - m_z
        J_11or24 = twoSEq_3  # J_11 negated in matrix multiplication
        J_12or23 = 2.0 * self.q3
        J_13or22 = twoSEq_1  # J_12negated in matrix multiplication
        J_14or21 = twoSEq_2
        J_32 = 2.0 * J_14or21  # negated in matrix multiplication
        J_33 = 2.0 * J_11or24  # negated in matrix multiplication
        J_41 = twob_zSEq_3  # negated in matrix multiplication
        J_42 = twob_zSEq_4
        J_43 = 2.0 * twob_xSEq_3 + twob_zSEq_1  # negated in matrix multiplication
        J_44 = 2.0 * twob_xSEq_4 - twob_zSEq_2  # negated in matrix multiplication
        J_51 = twob_xSEq_4 - twob_zSEq_2  # negated in matrix multiplication
        J_52 = twob_xSEq_3 + twob_zSEq_1
        J_53 = twob_xSEq_2 + twob_zSEq_4
        J_54 = twob_xSEq_1 - twob_zSEq_3  # negated in matrix multiplication
        J_61 = twob_xSEq_3
        J_62 = twob_xSEq_4 - 2.0 * twob_zSEq_2
        J_63 = twob_xSEq_1 - 2.0 * twob_zSEq_3
        J_64 = twob_xSEq_2

        # compute the gradient(matrix multiplication)

        SEqHatDot_1 = J_14or21 * f_2 - J_11or24 * f_1 - J_41 * f_4 - J_51 * f_5 + J_61 * f_6 
        SEqHatDot_2 = J_12or23 * f_1 + J_13or22 * f_2 - J_32 * f_3 + J_42 * f_4 + J_52 * f_5 + J_62 * f_6 
        SEqHatDot_3 = J_12or23 * f_2 - J_33 * f_3 - J_13or22 * f_1 - J_43 * f_4 + J_53 * f_5 + J_63 * f_6 
        SEqHatDot_4 = J_14or21 * f_1 + J_11or24 * f_2 - J_44 * f_4 - J_54 * f_5 + J_64 * f_6 
        
        # normalise the gradient to estimate direction of the gyroscope error
        norm = math.sqrt(  SEqHatDot_1 * SEqHatDot_1 + SEqHatDot_2 * SEqHatDot_2 + SEqHatDot_3 * SEqHatDot_3 + SEqHatDot_4 * SEqHatDot_4)
        SEqHatDot_1 = SEqHatDot_1 / norm 
        SEqHatDot_2 = SEqHatDot_2 / norm
        SEqHatDot_3 = SEqHatDot_3 / norm 
        SEqHatDot_4 = SEqHatDot_4 / norm 
        
        # compute angular estimated direction of the gyroscope error
        w_err_x = twoSEq_1 * SEqHatDot_2 - twoSEq_2 * SEqHatDot_1 - twoSEq_3 * SEqHatDot_4 + twoSEq_4 * SEqHatDot_3 
        w_err_y = twoSEq_1 * SEqHatDot_3 + twoSEq_2 * SEqHatDot_4 - twoSEq_3 * SEqHatDot_1 - twoSEq_4 * SEqHatDot_2 
        w_err_z = twoSEq_1 * SEqHatDot_4 - twoSEq_2 * SEqHatDot_3 + twoSEq_3 * SEqHatDot_2 - twoSEq_4 * SEqHatDot_1 

        # compute and remove the gyroscope baises
        self.w_bx += w_err_x * self.dt * self.zeta
        self.w_by += w_err_y * self.dt * self.zeta
        self.w_bz += w_err_z * self.dt * self.zeta
        g_x -= self.w_bx
        g_y -= self.w_by
        g_z -= self.w_bz

        # compute the quaternion rate measured by gyroscopes
        SEqDot_omega_1 = -halfSEq_2 * g_x - halfSEq_3 * g_y - halfSEq_4 * g_z
        SEqDot_omega_2 = halfSEq_1 * g_x + halfSEq_3 * g_z - halfSEq_4 * g_y
        SEqDot_omega_3 = halfSEq_1 * g_y - halfSEq_2 * g_z + halfSEq_4 * g_x
        SEqDot_omega_4 = halfSEq_1 * g_z + halfSEq_2 * g_y - halfSEq_3 * g_x

        # compute then integrate the estimated quaternion rate
        self.q0 += (SEqDot_omega_1 - (self.beta * SEqHatDot_1)) * self.dt
        self.q1 += (SEqDot_omega_2 - (self.beta * SEqHatDot_2)) * self.dt
        self.q2 += (SEqDot_omega_3 - (self.beta * SEqHatDot_3)) * self.dt
        self.q3 += (SEqDot_omega_4 - (self.beta * SEqHatDot_4)) * self.dt

        # normalise quaternion
        norm = math.sqrt(self.q0 * self.q0 + self.q1 * self.q1 + self.q2 * self.q2 + self.q3 * self.q3)
        self.q0 /= norm
        self.q1 /= norm
        self.q2 /= norm
        self.q3 /= norm

        # calculate euler angle from quaternion
        self.roll, self.pitch, self.yaw = rotation.anglesFromQuaternion(self.q0, self.q1, self.q2, self.q3,
                                                                        inDegrees=True)

        # compute flux in the earth frame

        SEq_1SEq_2 = self.q0 * self.q1  # recompute axulirary variables
        SEq_1SEq_3 = self.q0 * self.q2
        SEq_1SEq_4 = self.q0 * self.q3
        SEq_3SEq_4 = self.q2 * self.q3
        SEq_2SEq_3 = self.q1 * self.q2
        SEq_2SEq_4 = self.q1 * self.q3
        h_x = twom_x * (0.5 - self.q2 * self.q2 - self.q3 * self.q3) + twom_y * (SEq_2SEq_3 - SEq_1SEq_4) + twom_z * (
                    SEq_2SEq_4 + SEq_1SEq_3) 
        h_y = twom_x * (SEq_2SEq_3 + SEq_1SEq_4) + twom_y * (0.5 - self.q1 * self.q1 - self.q3 * self.q3) + twom_z * (
                    SEq_3SEq_4 - SEq_1SEq_2) 
        h_z = twom_x * (SEq_2SEq_4 - SEq_1SEq_3) + twom_y * (SEq_3SEq_4 + SEq_1SEq_2) + twom_z * (0.5 - self.q1 * self.q1 - self.q2 * self.q2)

        # normalise the flux vector to have only components in the x and z
        self.b_x = math.sqrt((h_x * h_x) + (h_y * h_y))
        self.b_z = h_z

    def simulationIMU_update(self, gx, gy, gz, ax, ay, az):
        self.updateIMU(math.degrees(gx), math.degrees(gy), math.degrees(gz), ax, ay, az, True)

        return self.roll, self.pitch, self.yaw

    def simulationMARG_update(self, gx, gy, gz, ax, ay, az, mx, my, mz):
        self.updateMARG_withBiasAndMagneticFieldCorrection(math.degrees(gx), math.degrees(gy), math.degrees(gz), ax, ay, az, mx, my, mz)

        return self.roll, self.pitch, self.yaw

    def changeDataRate(self, newDataRate):
        self.dt = newDataRate