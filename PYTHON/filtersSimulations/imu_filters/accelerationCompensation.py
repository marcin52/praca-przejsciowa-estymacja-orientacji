import math
import sys

import numpy as np


class AccelComp:

    def __init__(self, sigma_x, sigma_y, sigma_z, gravity_vec, k, Theta):

        self.sigma_x = sigma_x
        self.sigma_y = sigma_y
        self.sigma_z = sigma_z

        self.R_matrix = np.array([[self.sigma_x ** 2, 0, 0],
                            [0, self.sigma_y ** 2, 0],
                            [0, 0, self.sigma_z**2]])

        self.alfa_reference = math.sqrt(sigma_x**2 + sigma_y**2 + sigma_z**2) / 50

        self.gravity_vec = gravity_vec

        self.k = k
        self.Theta = Theta

    def getR_Matrix(self, ax, ay, az):

        #alfa = math.sqrt( (ax - self.gravity_vec[0])**2 + (ay - self.gravity_vec[1])**2 + (az - self.gravity_vec[2])**2 )

        alfa = math.sqrt(ax**2 + ay**2 + az**2) - 1

        # sys.stdout.write(f'\r{alfa}')
        # sys.stdout.flush()

        if alfa < self.alfa_reference:

            return self.R_matrix

        elif alfa < self.Theta:

            return self.R_matrix + np.array([[self.k * alfa ** 2,   0,                  0],
                                            [0,                     self.k * alfa ** 2, 0],
                                            [0,                     0,                  self.k * (alfa/self.gravity_vec[2])**2]])

        else:

            return np.array([[self.Theta, 0, 0],
                            [0, self.Theta, 0],
                            [0, 0, self.Theta]])