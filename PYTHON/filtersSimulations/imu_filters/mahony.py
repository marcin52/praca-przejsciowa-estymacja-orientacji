import math
import sys

from helpers import rotation
import numpy as np
from imu_filters.filters_helpers import normalize, radToDeg
from imu_filters.filters_helpers import degToRad
from imu_filters.filters_helpers import vectorCross
from imu_filters.filters_helpers import integrate

class MahonyFilter():

    def __init__(self, dt, Kp, Ki, roll=0, pitch=0, yaw=0):
        self.dt = dt
        self.halfT = dt / 2
        self.Kp = Kp
        self.Ki = Ki

        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

        self.rotationMatrix = rotation.computeRotationMatrixFromAngles(roll, pitch, yaw)

        self.exInt = 0
        self.eyInt = 0
        self.ezInt = 0

        self.integralFBx = 0
        self.integralFBy = 0
        self.integralFBz = 0

        #self.q0, self.q1, self.q2, self.q3 = rotation.quaternionFromRotationMatrix(self.rotationMatrix)
        self.q0 = 1
        self.q1 = 0
        self.q2 = 0
        self.q3 = 0
    # gyro data in radians
    def updateIMU(self, gyrox, gyroy, gyroz, accx, accy, accz):

        # preprocess data
        gyrox, gyroy, gyroz = degToRad(gyrox, gyroy, gyroz)

        # Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if not ((accx == 0.0) and (accy == 0.0) and (accz == 0.0)):

            accx, accy, accz = normalize(accx, accy, accz)

            # through vector cross product to show measured attitude error of gyroscope
            ex, ey, ez = vectorCross(np.array([accx, accy, accz]), self.rotationMatrix[2])

            self.integrateError(ex, ey, ez)

            # main algorithm
            self.updateQuaternion(gyrox, gyroy, gyroz, ex, ey, ez)
        else:
            self.integrateQuaternion( gyrox, gyroy, gyroz)

        # calculate rotation matrix
        self.rotationMatrix = rotation.computeRotationMatrixFromQuaternions(self.q0, self.q1, self.q2, self.q3)

        # calculate euler angles from quaternion
        self.roll, self.pitch, self.yaw = rotation.anglesFromQuaternion(self.q0, self.q1, self.q2, self.q3, inDegrees=True)

    def integrateError(self, ex, ey, ez):
        self.exInt += self.Ki * ex * self.dt
        self.eyInt += self.Ki * ey * self.dt
        self.ezInt += self.Ki * ez * self.dt

    def updateQuaternion(self, gyrox, gyroy, gyroz, ex, ey, ez):

        # PI controller to fuse data
        gyrox += self.Kp * ex + self.exInt
        gyroy += self.Kp * ey + self.eyInt
        gyroz += self.Kp * ez + self.ezInt

        self.integrateQuaternion(gyrox, gyroy, gyroz)

    def integrateQuaternion(self, gyrox, gyroy, gyroz):
        # update quaternion through Runge–Kutta methods in discrete system
        q0Last = self.q0
        q1Last = self.q1
        q2Last = self.q2
        q3Last = self.q3

        self.q0 += (-q1Last * gyrox - q2Last * gyroy - q3Last * gyroz) * self.halfT
        self.q1 += (q0Last * gyrox + q2Last * gyroz - q3Last * gyroy) * self.halfT
        self.q2 += (q0Last * gyroy - q1Last * gyroz + q3Last * gyrox) * self.halfT
        self.q3 += (q0Last * gyroz + q1Last * gyroy - q2Last * gyrox) * self.halfT

        self.q0, self.q1, self.q2, self.q3 = normalize(self.q0, self.q1, self.q2, self.q3)

    def updateMARG(self, gyrox, gyroy, gyroz, accx, accy, accz, mx, my, mz):

        q0 = self.q0
        q1 = self.q1
        q2 = self.q2
        q3 = self.q3
        # Use IMU algorithm if magnetometer measurement invalid(avoids NaN in magnetometer normalisation)
        if (mx == 0.0) and (my == 0.0) and (mz == 0.0):
            self.updateIMU(gyrox, gyroy, gyroz, accx, accy, accz)
            return

        gyrox, gyroy, gyroz = degToRad(gyrox, gyroy, gyroz)

        # Compute feedback only if accelerometer measurement valid (avoids NaN in accelerometer normalisation)
        if not ((accx == 0.0) and (accy == 0.0) and (accz == 0.0)):

            accx, accy, accz = normalize(accx, accy, accz)

            mx, my, mz = normalize(mx, my, mz)

            q0q0 = self.q0 * self.q0
            q0q1 = self.q0 * self.q1
            q0q2 = self.q0 * self.q2
            q0q3 = self.q0 * self.q3
            q1q1 = self.q1 * self.q1
            q1q2 = self.q1 * self.q2
            q1q3 = self.q1 * self.q3
            q2q2 = self.q2 * self.q2
            q2q3 = self.q2 * self.q3
            q3q3 = self.q3 * self.q3

            # Reference direction of Earth's magnetic field
            hx = 2.0 * (mx * (0.5 - q2q2 - q3q3) + my * (q1q2 - q0q3) + mz * (q1q3 + q0q2))
            hy = 2.0 * (mx * (q1q2 + q0q3) + my * (0.5 - q1q1 - q3q3) + mz * (q2q3 - q0q1))
            bx = math.sqrt(hx * hx + hy * hy)
            bz = 2.0 * (mx * (q1q3 - q0q2) + my * (q2q3 + q0q1) + mz * (0.5 - q1q1 - q2q2))

            # Estimated direction of gravity and magnetic field
            vx = 2 * (q1q3 - q0q2)
            vy = 2 * (q0q1 + q2q3)
            #vz = q0q0 - 0.5 + q3q3
            vz = q0q0 - q1q1 - q2q2 + q3q3
            wx = 2 * bx * (0.5 - q2q2 - q3q3) + bz * (q1q3 - q0q2)
            wy = 2 * bx * (q1q2 - q0q3) + bz * (q0q1 + q2q3)
            wz = 2 * bx * (q0q2 + q1q3) + bz * (0.5 - q1q1 - q2q2)

            ex = (accy * vz - accz * vy) + (my * wz - mz * wy)
            ey = (accz * vx - accx * vz) + (mz * wx - mx * wz)
            ez = (accx * vy - accy * vx) + (mx * wy - my * wx)
            if (self.Ki > 0.0):

                self.integralFBx += ex
                self.integralFBy += ey
                self.integralFBz += ez
            else:
                self.integralFBx = 0.0
                self.integralFBy = 0.0
                self.integralFBz = 0.0


            gyrox = gyrox + self.Kp * ex + self.Ki * self.integralFBx
            gyroy = gyroy + self.Kp * ey + self.Ki * self.integralFBy
            gyroz = gyroz + self.Kp * ez + self.Ki * self.integralFBz

                # Error is sum of cross product between estimated direction
            # and measured direction of field vectors
            # halfex = (accy * halfvz - accz * halfvy) + (my * halfwz - mz * halfwy)
            # halfey = (accz * halfvx - accx * halfvz) + (mz * halfwx - mx * halfwz)
            # halfez = (accx * halfvy - accy * halfvx) + (mx * halfwy - my * halfwx)
            #
            # # integral error scaled by Ki
            # self.integralFBx += 2 * self.Ki * halfex * self.dt
            # self.integralFBy += 2 * self.Ki * halfey * self.dt
            # self.integralFBz += 2 * self.Ki * halfez * self.dt
            # gyrox += self.integralFBx # apply integral feedback
            # gyroy += self.integralFBy
            # gyroz += self.integralFBz
            #
            # # Apply  proportional feedback
            # gyrox += 2 * self.Kp * halfex
            # gyroy += 2 * self.Kp * halfey
            # gyroz += 2 * self.Kp * halfez

        # Integrate rate of change of quaternion
        # gyrox *= (0.5 * self.dt) # pre - multiply common factors
        # gyroy *= (0.5 * self.dt)
        # gyroz *= (0.5 * self.dt)
        # qa = self.q0
        # qb = self.q1
        # qc = self.q2
        # self.q0 += (-qb * gyrox - qc * gyroy - self.q3 * gyroz)
        # self.q1 += (qa * gyrox + qc * gyroz - self.q3 * gyroy)
        # self.q2 += (qa * gyroy - qb * gyroz + self.q3 * gyrox)
        # self.q3 += (qa * gyroz + qb * gyroy - qc * gyrox)
        pa = q1
        pb = q2
        pc = q3
        # self.q0 += (-self.q1 * gyrox - self.q2 * gyroy - self.q3 * gyroz)*(0.5 * self.dt)
        # self.q1 += (self.q0 * gyrox + qb * gyroz - qc * gyroy)*(0.5 * self.dt)
        # self.q2 += (self.q0 * gyroy - qa * gyroz + qc * gyrox)*(0.5 * self.dt)
        # self.q3 += (self.q0 * gyroz + qa * gyroy - qb * gyrox)*(0.5 * self.dt)

        q0 = q0 + (-q1 * gyrox - q2 * gyroy - q3 * gyroz) * (0.5 * self.dt)
        q1 = pa + (q0 * gyrox + pb * gyroz - pc * gyroy) * (0.5 * self.dt)
        q2 = pb + (q0 * gyroy - pa * gyroz + pc * gyrox) * (0.5 * self.dt)
        q3 = pc + (q0 * gyroz + pa * gyroy - pb * gyrox) * (0.5 * self.dt)
        #self.q0, self.q1, self.q2, self.q3 = normalize(self.q0, self.q1, self.q2, self.q3)

        norm = 1 / math.sqrt(q0**2 + q1**2 + q2**2 + q3**2)
        self.q0 = q0 * norm
        self.q1 = q1 * norm
        self.q2 = q2 * norm
        self.q3 = q3 * norm

        self.roll, self.pitch, self.yaw = rotation.anglesFromQuaternion(self.q0, self.q1, self.q2, self.q3,
                                                                        inDegrees=True)


    def simulationIMU_update(self, gx, gy, gz, ax, ay, az ):
        self.updateIMU(math.degrees(gx), math.degrees(gy), math.degrees(gz), ax, ay, az)

        return self.roll, self.pitch, self.yaw

    def simulationMARG_update(self, gx, gy, gz, ax, ay, az, mx, my, mz):

        # sys.stdout.write(f'\r{mx},{my},{mz}')
        # sys.stdout.flush()
        #self.updateMARG(math.degrees(gx), math.degrees(gy), math.degrees(gz), ax, ay, az, mx, my, mz)
        self.updateMARG(math.degrees(gx), math.degrees(gy), math.degrees(gz), ax, ay, az, mx, my, mz)

        sys.stdout.write(f'\r{self.q0},{self.q1},{self.q2}, {self.q3}')
        sys.stdout.flush()

        return self.roll, self.pitch, self.yaw

    def changeDataRate(self, newDataRate):
        self.dt = newDataRate