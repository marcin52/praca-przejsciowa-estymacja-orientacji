from helpers import rotation

import math
import numpy as np
from filterpy.kalman import MerweScaledSigmaPoints, JulierSigmaPoints
from filterpy.kalman import UnscentedKalmanFilter as UKF
from numpy import array
from imu_filters.filters_helpers import normalize

class QuaternionUKF:

    def __init__(self, dt, gyro_sigma, acc_sigma, mag_sigma=0, roll=0, pitch=0, yaw=0, MARG_filt=False, g_const=1,
                 estimateBias=False):

        self.dt = dt
        self.g_const = g_const
        self.h_const = 1
        self.alfa = 0

        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

        self.MARG_filt = MARG_filt

        self.gyro_sigma = gyro_sigma

        transitionFunc = self.fx
        if estimateBias:
            transitionFunc = self.fx_gyroBiasEstimate

        if self.MARG_filt:
            self.ukf = self.initMARG_UKF(dt, gyro_sigma, acc_sigma, mag_sigma, transitionFunc)
        else:
            self.ukf = self.initIMU_UKF(dt, gyro_sigma, acc_sigma, transitionFunc)

        # Magnetic vector constant(align with local magnetic vector) * /
        self.IMU_MAG_B0 = [math.cos(0), math.sin(0), 0.000000]

    def updateIMU(self, gyrox, gyroy, gyroz, accx, accy, accz, returnAngles=True):

        self.ukf.predict(wx=gyrox, wy=gyroy, wz=gyroz)
        self.ukf.update(z=(accx, accy, accz))

        if returnAngles:
            return self.getAngles()
        else:
            return self.ukf.x

    def updateMARG(self, gyrox, gyroy, gyroz, accx, accy, accz, mx, my, mz, returnAngles=True):

        mx, my, mz = normalize(mx, my, mz)
        accx, accy, accz = normalize(accx, accy, accz)

        qx = self.ukf.x[0]
        qy = self.ukf.x[1]
        qz = self.ukf.x[2]
        qw = self.ukf.x[3]
        W = self.dt / 2 * array([[-qx, -qy, -qz],
                  [qw, -qz, qy],
                  [qz, qw, -qx],
                  [-qy, qx, qw]])

        Q = np.dot(W,  W.T) * self.gyro_sigma

        self.ukf.Q[0:4, 0:4] = Q

        self.ukf.predict(wx=gyrox, wy=gyroy, wz=gyroz)
        self.ukf.update(z=(accx, accy, accz, mx, my, mz))
        #self.ukf.P *= 0.95

        if returnAngles:
            return self.getAngles()
        else:
            return self.ukf.x

    def fx_gyroBiasEstimate(self, x, dt, wx, wy, wz):
        q0Last = x[0]
        q1Last = x[1]
        q2Last = x[2]
        q3Last = x[3]

        x_new = array([0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0])

        wx -= x[4]
        wy -= x[5]
        wz -= x[6]

        x_new[0] = q0Last + (-q1Last * wx - q2Last * wy - q3Last * wz) * dt / 2.
        x_new[1] = q1Last + (q0Last * wx + q2Last * wz - q3Last * wy) * dt / 2.
        x_new[2] = q2Last + (q0Last * wy - q1Last * wz + q3Last * wx) * dt / 2.
        x_new[3] = q3Last + (q0Last * wz + q1Last * wy - q2Last * wx) * dt / 2.
        x_new[4] = x[4]
        x_new[5] = x[5]
        x_new[6] = x[6]

        return x_new

    def fx(self, x, dt, wx, wy, wz):
        q0Last = x[0]
        q1Last = x[1]
        q2Last = x[2]
        q3Last = x[3]

        x_new = array([0.0, 0.0, 0.0, 0.0])

        x_new[0] = q0Last + (-q1Last * wx - q2Last * wy - q3Last * wz) * dt / 2.
        x_new[1] = q1Last + (q0Last * wx + q2Last * wz - q3Last * wy) * dt / 2.
        x_new[2] = q2Last + (q0Last * wy - q1Last * wz + q3Last * wx) * dt / 2.
        x_new[3] = q3Last + (q0Last * wz + q1Last * wy - q2Last * wx) * dt / 2.

        return x_new

    def hx_acc(self, x):

        a1 = (2 * x[1] * x[3] - 2 * x[0] * x[2]) * self.g_const
        a2 = (2 * x[2] * x[3] + 2 * x[0] * x[1]) * self.g_const
        a3 = (x[0] ** 2 - x[1] ** 2 - x[2] ** 2 + x[3] ** 2) * self.g_const
        res_acc = array([a1, a2, a3])

        return res_acc

    def hx_acc_mag(self, x):

        C = rotation.computeRotationMatrixFromQuaternions2(x[0], x[1], x[2], x[3])

        G = array([0, 0, self.g_const]).T
        R = array([0, math.cos(math.radians(14.3)), -math.sin(math.radians(14.3))]).T

        A = np.dot(C, G)
        M = np.dot(C, R)

        res_acc = array([A[0], A[1], A[2], M[0], M[1], M[2]])

        return res_acc

    def initIMU_UKF(self, dt, acc_sigma, gyro_sigma, transitionFunc):

        n = 4
        kappa = -1
        if transitionFunc == self.fx_gyroBiasEstimate:
            n = 7
            kappa = -4

        sigmas = MerweScaledSigmaPoints(n=n, alpha=0.01, beta=2, kappa=kappa)
        # sigmas = JulierSigmaPoints(n=4, kappa=-1)

        ukf = UKF(dim_x=n, dim_z=3, dt=dt, hx=self.hx_acc, fx=transitionFunc, points=sigmas)

        matrix = rotation.computeRotationMatrixFromAngles(self.roll, self.pitch, self.yaw)
        q1, q2, q3, q4 = rotation.quaternionFromRotationMatrix(matrix)

        ukf.x[0] = q1
        ukf.x[1] = q2
        ukf.x[2] = q3
        ukf.x[3] = q4

        ukf.P *= 1.
        ukf.R *= acc_sigma * 0.1
        ukf.Q *= 0.000001

        if transitionFunc == self.fx_gyroBiasEstimate:
            ukf.Q[4][4] *= 20
            ukf.Q[5][5] *= 20
            ukf.Q[6][6] *= 20

        return ukf

    def initMARG_UKF(self, dt, acc_sigma, magn_sigma, gyro_sigma, transitionFunc):

        n = 4
        kappa = -1
        if transitionFunc == self.fx_gyroBiasEstimate:
            n = 7
            kappa = -4

        #sigmas = MerweScaledSigmaPoints(n=n, alpha=0.9, beta=2, kappa=kappa)
        sigmas = JulierSigmaPoints(n=7, kappa=-4)

        ukf = UKF(dim_x=n, dim_z=6, dt=dt, hx=self.hx_acc_mag, fx=transitionFunc, points=sigmas)

        matrix = rotation.computeRotationMatrixFromAngles(self.roll, self.pitch, self.yaw)
        q1, q2, q3, q4 = rotation.quaternionFromRotationMatrix(matrix)

        ukf.x[0] = q1
        ukf.x[1] = q2
        ukf.x[2] = q3
        ukf.x[3] = q4

        ukf.P *= 1.
        ukf.R = array([[acc_sigma, 0., 0., 0., 0., 0.],
                       [0., acc_sigma, 0., 0., 0., 0.],
                       [0., 0., acc_sigma, 0., 0., 0.],
                       [0., 0., 0., magn_sigma, 0., 0.],
                       [0., 0., 0., 0., magn_sigma, 0.],
                       [0., 0., 0., 0., 0., magn_sigma]])

        #ukf.R *= acc_sigma * 0.1
        ukf.Q *= 0.000001

        if transitionFunc == self.fx_gyroBiasEstimate:
            ukf.Q[4][4] *= 20
            ukf.Q[5][5] *= 20
            ukf.Q[6][6] *= 20

        return ukf

    def getAngles(self):

        q1 = self.ukf.x[0]
        q2 = self.ukf.x[1]
        q3 = self.ukf.x[2]
        q4 = self.ukf.x[3]

        self.roll, self.pitch, self.yaw = rotation.anglesFromQuaternion(q1, q2, q3, q4)
        return math.degrees(self.roll), math.degrees(self.pitch), math.degrees(self.yaw)

    def changeDataRate(self, newDataRate):
        self.dt = newDataRate
