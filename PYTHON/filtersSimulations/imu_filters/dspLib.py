import numpy as num
import matplotlib.pyplot as plt
from scipy.signal import butter, filtfilt, lfilter_zi, lfilter, cheby1
import math
import random

def generateValues(size, trueValue, delta):

    array = num.ndarray(size, float)

    for i in range(0, size):
        array[i] = random.uniform(trueValue - delta, trueValue + delta)

    return array

def generateVelocities(size, positions, delta, timeSample):

    array = num.ndarray(size, float)

    for i in range(1, size):
        array[i] = (positions[i] - positions[i - 1]) / timeSample + random.uniform(-delta,  delta)

    return array

def lowPassFilter(data, beta_coeff):
    size = len(data)
    filtered = num.ndarray(size, float)

    filtered[0] = data[0]

    for i in range(1, size):
        filtered[i] = filtered[i - 1] - (beta_coeff * (filtered[i - 1] - data[i]))

    return filtered

def butter_lowpass_filter(data, cutoff, sampleRate, order=5):
    nyq = 0.5 * sampleRate
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

class SimpleLowPassFilter():

    def __init__(self, initValue, beta_coeff):
        self.value = initValue
        self.beta = beta_coeff

    def filterStep(self, newRawValue):
        #self.value = self.value - (self.beta * (self.value - newRawValue))
        self.value = (1 - self.beta) * newRawValue + self.beta * self.value
        return self.value

class SimpleHighPassFilter():

    def __init__(self, initValue, beta_coeff):
        self.value = initValue
        self.old_rawValue = initValue
        self.beta = beta_coeff
        self.counter = 0

    def filterStep(self, newRawValue):

        if self.counter == 0:
            self.counter = 1
            self.old_rawValue = newRawValue
        else:
            self.value = (1 - self.beta) * self.value + self.beta * (newRawValue - self.old_rawValue)

        return self.value

class ButterworthLowPassFilter():

    def __init__(self, cutoff, sampleRate, order=5, initialValue = 1):
        self.cutoff = cutoff
        self.sampleRate = sampleRate

        nyq = 0.5 * sampleRate
        normal_cutoff = cutoff / nyq
        self.b, self.a = butter(order, normal_cutoff, btype='low', analog=False)

        self.filterState = lfilter_zi(self.b, self.a) * initialValue
        self.result = 0

    def filter_step(self, data):
        self.result, self.filterState = lfilter(self.b, self.a, [data], zi=self.filterState)

        return self.result


class Chebyshev1LowPassFilter():

    def __init__(self, cutoff, maxRiple, sampleRate, order=5, initialValue = 1):
        self.cutoff = cutoff
        self.sampleRate = sampleRate

        nyq = 0.5 * sampleRate
        normal_cutoff = cutoff / nyq
        self.b, self.a = cheby1(order, maxRiple, normal_cutoff, btype='low', analog=False)

        self.filterState = lfilter_zi(self.b, self.a) * initialValue
        self.result = 0

    def filter_step(self, data):
        self.result, self.filterState = lfilter(self.b, self.a, [data], zi=self.filterState)

        return self.result
