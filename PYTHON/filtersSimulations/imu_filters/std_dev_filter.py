import math
import statistics


class StdDevFilter:

    def __init__(self):

        self.a1 = 0
        self.a2 = 0
        self.a3 = 0
        self.counter = 0

    def step(self, value):

        if self.counter == 0:
            self.a3 = value
            self.counter += 1

            return value

        elif self.counter == 1:
            self.a2 = value
            self.counter += 1

            return value

        elif self.counter == 2:
            self.a1 = value
            self.counter = 0

            self.calcStdDev()
            return self.a3

        # elif self.counter == 3:
        #
        #     self.a3 = self.a2
        #     self.a2 = self.a1
        #     self.a1 = value
        #
        #     self.calcStdDev()
        #     return self.a3

    def calcStdDev(self):

        sample = [self.a1, self.a2, self.a3]
        dev = statistics.stdev(sample)

        avg = (self.a1 + self.a2 + self.a3) / 3

        if math.fabs( avg - self.a1) > 1. * dev:
            self.a1 = (self.a2 + self.a3) / 2

        if math.fabs( avg - self.a2) > 1. * dev:
            self.a1 = (self.a1 + self.a3) / 2

        if math.fabs( avg - self.a3) > 1. * dev:
            self.a1 = (self.a2 + self.a1) / 2

        # for i in range(0 , 3):
        #
        #     if math.abs( avg - sample[i]) > dev:


