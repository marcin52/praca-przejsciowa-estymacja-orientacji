from helpers import rotation

import math
from filterpy.kalman import KalmanFilter
from numpy import array
import numpy as np

class DCM_KF:

    def __init__(self, dt, gyro_sigma, acc_sigma, mag_sigma=0, roll = 0, pitch = 0, yaw = 0, MARG_filt=False, g_const=1):

        self.dt = dt
        self.g_const = g_const

        self.roll = roll
        self.pitch = pitch
        self.yaw = yaw

        self.MARG_filt = MARG_filt

        if self.MARG_filt:
            self.kf =  self.initMARG_KF(dt, gyro_sigma, acc_sigma, mag_sigma)
        else:
            self.kf = self.initIMU_KF(dt, gyro_sigma, acc_sigma)

    def updateIMU(self, gyrox, gyroy, gyroz, accx, accy, accz, returnAngles=True):

        self.kf.F = array([[1., 0., 0., 0., float(-self.kf.x[2]) * self.dt / 2, float(self.kf.x[1]) * self.dt/2],
                          [0., 1., 0., float(self.kf.x[2]) * self.dt/2, 0, float(-self.kf.x[0]) * self.dt/2],
                          [0., 0., 1., float(-self.kf.x[1]) * self.dt/2, float(self.kf.x[0]) * self.dt/2, 0.],
                          [0., 0., 0., 1., 0., 0.],
                          [0., 0., 0., 0., 1., 0.],
                          [0., 0., 0., 0., 0., 1.]])

        self.kf.predict()
        self.kf.update(z=(accx, accy, accz, gyrox, gyroy, gyroz))

        norm = math.sqrt(float(self.kf.x[0]) ** 2 + float(self.kf.x[1]) ** 2 + float(self.kf.x[2]) ** 2)
        self.kf.x[0] /= norm
        self.kf.x[1] /= norm
        self.kf.x[2] /= norm

        if returnAngles:
            return self.getAngles()
        else:
            return self.kf.x

    def updateMARG(self, gyrox, gyroy, gyroz, accx, accy, accz, mx, my, mz, returnAngles=False):

        pass

    def initIMU_KF(self, dt, gyro_sigma, acc_sigma):

        kf = KalmanFilter(dim_x=6, dim_z=6, dim_u=0.)

        kf.F = array([[1., 0., 0., 0., dt, dt],
                      [0., 1., 0., dt, 0., dt],
                      [0., 0., 1., dt, dt, 0.],
                      [0., 0., 0., 1., 0., 0.],
                      [0., 0., 0., 0., 1., 0.],
                      [0., 0., 0., 0., 0., 1.]])

        matrix = rotation.computeRotationMatrixFromAngles(self.roll, self.pitch, self.yaw)
        kf.x[0] = matrix[0][2]
        kf.x[1] = matrix[1][2]
        kf.x[2] = matrix[2][2]

        kf.H = array([[self.g_const, 0.,            0.,             0., 0., 0.],
                      [0.,          self.g_const,   0.,             0., 0., 0.],
                      [0.,          0.,             self.g_const,   0., 0., 0.],
                      [0.,          0.,             0.,             1., 0., 0.],
                      [0.,          0.,             0.,             0., 1., 0.],
                      [0.,          0.,             0.,             0., 0., 1.]])

#        gyro_sigma = 0.01
        kf.Q = array([[0., 0., 0., 0., 0., 0.],
                      [0., 0., 0., 0., 0., 0.],
                      [0., 0., 0., 0., 0., 0.],
                      [0., 0., 0., gyro_sigma, 0., 0.],
                      [0., 0., 0., 0., gyro_sigma, 0.],
                      [0., 0., 0., 0., 0., gyro_sigma]])

        # kf.Q[3:6, 3:6] = Q_discrete_white_noise(3, dt=dt, var=1.0)
        kf.P *= 1.0
        kf.R = array([[acc_sigma, 0., 0., 0., 0., 0.],
                      [0., acc_sigma, 0., 0., 0., 0.],
                      [0., 0., acc_sigma, 0., 0., 0.],
                      [0., 0., 0., gyro_sigma, 0., 0.],
                      [0., 0., 0., 0., gyro_sigma, 0.],
                      [0., 0., 0., 0., 0., gyro_sigma]]) * 0.1
        return kf

    def initMARG_KF(self, dt, acc_sigma, magn_sigma, gyro_sigma):
        pass

    def getAngles(self):

        self.pitch = np.rad2deg(-math.asin(self.kf.x[0]))
        self.roll = np.rad2deg(math.atan2(self.kf.x[1], self.kf.x[2]))
        self.yaw = 0

        return self.roll, self.pitch, self.yaw

    def changeDataRate(self, newDataRate):
        self.dt = newDataRate
