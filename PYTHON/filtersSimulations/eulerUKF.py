import math

import numpy
from filterpy.common import Q_discrete_white_noise
from filterpy.kalman import MerweScaledSigmaPoints, JulierSigmaPoints
from filterpy.kalman import UnscentedKalmanFilter as UKF
from numpy import array


def fx(x, dt, wx, wy, wz):
   # F = array([[1., 0., 0., math.cos(x[1]) * dt, -math.sin(x[1]) * dt],
   #            [0., 1., 1., math.sin(x[1]) * math.tan(x[0]) * dt, math.cos(x[1]) * math.tan(x[0]) * dt],
   #            [0., 0., 1., 0., 0.],
   #            [0., 0., 0., 1., 0.],
   #            [0., 0., 0., 0., 1.]])
    F = array([[ 1., 0.,  1. * dt, math.sin(x[0]) * math.tan(x[1]) * dt, math.cos(x[0]) * math.tan(x[1]) * dt],
                [ 0., 1., 0., math.cos(x[1])*dt, -math.sin(x[1])*dt],
                [ 0., 0., 1., 0., 0.],
                [ 0., 0., 0., 1., 0.],
                [ 0., 0., 0., 0., 1.]])

    z = x[0]
    y = x[1]
    E = array([[0, -math.sin(z), math.cos(y)*math.cos(z)],
               [0, math.cos(z), math.cos(y)*math.sin(z)],
              [1, 0, -math.sin(y)]])

    w = array([[wx - x[2]], [wy - x[3]], [wz - x[4]]])

    eul = numpy.matmul(numpy.linalg.inv(E), w)

    x_new = array([0.0, 0.0, 0.0, 0.0, 0.0])

    x_new[0] = x[0] + eul[0] * dt
    x_new[1] = x[1] + eul[1] * dt
    x_new[2] = x[2]
    x_new[3] = x[3]
    x_new[4] = x[4]

    # x_new[0] = x[0] + (wx - x[2]) * dt + (wy - x[3]) * math.sin(x[0]) * math.cos(x[1]) * dt + (wz - x[4]) * math.cos(x[0]) * math.cos(x[1]) * dt
    # x_new[1] = x[1] + (wy - x[3]) * math.cos(x[1])*dt - (wz - x[4]) * math.sin(x[1])*dt
    # x_new[2] = x[2]
    # x_new[3] = x[3]
    # x_new[4] = x[4]

    if x_new[0] > math.pi:
       x_new[0] -= 2*math.pi

    if x_new[0] < -math.pi:
        x_new[0] += 2*math.pi

    if x_new[1] > math.pi:
        x_new[1] -= 2*math.pi

    if x_new[1] < -math.pi:
        x_new[1] += 2*math.pi

    return x_new

def hx(x):

    res_acc = array([-math.sin(x[1]) * 1, math.sin(x[0])*math.cos(x[1]) * 1, math.cos(x[0])*math.cos(x[1]) * 1])

    return res_acc


def initUKF(dt, acc_sigma, magn_sigma, gyro_sigma):

    sigmas = MerweScaledSigmaPoints(n=5, alpha=0.01, beta=2, kappa=-2)
    #sigmas = JulierSigmaPoints(n=5, kappa=-2)

    ukf = UKF(dim_x=5, dim_z=3, dt=dt, hx=hx, fx=fx, points=sigmas)
    ukf.P *= 1
    #ukf.R = array([[ acc_sigma, 0., 0., 0., 0.],
    #              [ 0., acc_sigma, 0., 0., 0.],
    #              [ 0., 0., gyro_sigma, 0, 0.],
    #              [ 0., 0., 0., gyro_sigma, 0.],
    #              [ 0., 0., 0., 0., gyro_sigma]]) * 1

    ukf.x*= 0

    ukf.Q *= 0.00001
    #ukf.Q[2:5, 2:5] *= 10000
    #ukf.Q[2:5, 2:5] = Q_discrete_white_noise(3, dt=dt, var=gyro_sigma)
    #ukf.Q = Q_discrete_white_noise(4, dt=dt, var=gyro_sigma) * 1

    return ukf
