from scipy import signal
from scipy import rand
import numpy
import matplotlib.pyplot as plt

def filter_sbs():
    data = rand(2000)
    b = signal.firwin(150, 0.004)
    z = signal.lfilter_zi(b, 1)
    result = numpy.zeros(data.size)
    for i, x in enumerate(data):
        result[i], z = signal.lfilter(b, 1, [x], zi=z)

    plt.plot(data, 'r', label="raw")  # plotting t, a separately
    plt.plot(result, 'b', label="filtered")  # plotting t, b separately
    plt.legend(loc="upper left")
    plt.show()
    return result

if __name__ == '__main__':
    result = filter_sbs()
