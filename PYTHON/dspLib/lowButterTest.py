import numpy as np
from scipy.signal import butter,filtfilt
import matplotlib.pyplot as plt

# Filter requirements.
T = 5.0         # Sample Period
fs = 30.0       # sample rate, Hz
cutoff = 2      # desired cutoff frequency of the filter, Hz ,      slightly higher than actual 1.2 Hz
nyq = 0.5 * fs  # Nyquist Frequency
order = 2       # sin wave can be approx represented as quadratic
n = int(T * fs) # total number of samples

def butter_lowpass_filter(data, cutoff, fs, order):
    normal_cutoff = cutoff / nyq
    # Get the filter coefficients
    b, a = butter(order, normal_cutoff, btype='low', analog=False)
    y = filtfilt(b, a, data)
    return y

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    # sin wave

    sig = []
    noise = []
    for t in range(0, int(T * fs)):
        sig.append( np.sin(1.2 * 2 * np.pi * t))
        # Lets add some noise
        noise.append( 1.5 * np.cos(9 * 2 * np.pi * t) + 0.5 * np.sin(12.0 * 2 * np.pi * t))

    data = sig + noise

    # Filter the data, and plot both the original and filtered signals.
    y = butter_lowpass_filter(data, cutoff, fs, order)


    plt.plot(data[0:150], 'r', label="raw")  # plotting t, a separately
    plt.plot(y[0:150], 'b', label = "filtered")  # plotting t, b separately
    plt.legend(loc="upper left")
    plt.show()