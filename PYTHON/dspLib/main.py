# This is a sample Python script.

# Press Shift+F10 to execute it or replace it with your code.
# Press Double Shift to search everywhere for classes, files, tool windows, actions, and settings.

# import required modules
import numpy as num
import matplotlib.pyplot as plt
import random
import dspLib
import scipy

# Press the green button in the gutter to run the script.
if __name__ == '__main__':

    simulationSize = 1000
    timeSample = 0.01
    angle = 50

    random.seed(random.randint(0, 100))

    angles = dspLib.generateValues(int(simulationSize/2), angle, 2)
    angles2 = dspLib.generateValues(int(simulationSize / 2), angle, 2)
    angles = num.append(angles, angles2)
    omegas = dspLib.generateVelocities(simulationSize, angles, 1, timeSample)

    lowPassedAngles = num.ndarray(simulationSize, float)
    chebyshev1FilterAngles = num.ndarray(simulationSize, float)
    butterLowPassedAngles = num.ndarray(simulationSize, float)

    lowPassFilter = dspLib.SimpleLowPassFilter(angles[0], 0.1)
    chebyshev1Filter = dspLib.Chebyshev1LowPassFilter(5, 1, 1 / timeSample, 5, angles[0])
    butterFilter = dspLib.ButterworthLowPassFilter(5, 1 / timeSample, 5, angles[0])

    for i in range(0, simulationSize):
        butterLowPassedAngles[i] = butterFilter.filter_step(angles[i])
        chebyshev1FilterAngles[i] = chebyshev1Filter.filter_step(angles[i])
        lowPassedAngles[i] = lowPassFilter.filterStep(angles[i])

    print("low-pass variance:" + str(scipy.mean(lowPassedAngles)))
    print("butter variance:" + str(scipy.mean(butterLowPassedAngles)))
    print("chebyshev1 variance:" + str(scipy.mean(chebyshev1FilterAngles)))

    x = range(0, simulationSize)
    plt.plot( x, angles,'r--', label = "raw")  # plotting t, a separately
    plt.plot(x, butterLowPassedAngles, 'b', label = "butterworth")  # plotting t, b separately
    plt.plot(x, lowPassedAngles, 'g', label = "low pass")  # plotting t, c separately
    plt.plot(x, chebyshev1FilterAngles, 'y', label="chebyshev1")  # plotting t, c separately
    plt.legend(loc="upper left")
    plt.show()



# See PyCharm help at https://www.jetbrains.com/help/pycharm/
