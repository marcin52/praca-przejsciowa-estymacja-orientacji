fs = 100;

timeStamp1 = fs;
timeStamp2 = fs * 1;
timeStamp3 = fs * 2;
timeStamp4 = fs * 3;
timeStamp5 = fs * 4;
timeStamp6 = fs * 5;
timeStamp7 = fs * 6;
timeStamp8 = fs * 7;
timeStamp9 = fs * 8;
timeStamp10 = fs * 9;
timeStamp11 = fs * 10;

totalNumSamples = timeStamp1 + timeStamp11;

traj = kinematicTrajectory('SampleRate',fs);

accBody = zeros(totalNumSamples,3);

angVelBody = zeros(totalNumSamples,3);
% 
% angVelBody(1:timeStamp1,1) = 0;
% angVelBody(timeStamp1:timeStamp2,1) = -(2*pi)/40;
% angVelBody(timeStamp2:timeStamp3,1) = 0;
% % angVelBody(timeStamp3:timeStamp4,1) = (2*pi)/10;
% % angVelBody(timeStamp4:timeStamp5,1) = (2*pi)/5;
% % angVelBody(timeStamp5:timeStamp6,1) = (2*pi)/2;
% angVelBody(timeStamp3:timeStamp6,1) = linspace( 0, pi/2, 3*fs + 1);
% angVelBody(timeStamp6:timeStamp7,1) = linspace( pi/2, -pi/40, fs + 1);
% angVelBody(timeStamp7:timeStamp8,1) = -pi/40;
% angVelBody(timeStamp8:timeStamp9,1) = (2*pi)/40;
% angVelBody(timeStamp9:timeStamp10,1) = -(2*pi)/3;
% angVelBody(timeStamp10:timeStamp11,1) = -(2*pi)/40;
% angVelBody(timeStamp11:end,1) = 0;
% 
% angVelBody(1:timeStamp1,2) = -(2*pi)/4;
% angVelBody(timeStamp1:timeStamp2,2) = -(2*pi)/40;
% angVelBody(timeStamp2:timeStamp3,2) = 0;
% angVelBody(timeStamp3:timeStamp4,2) = linspace( 0, -pi/1.5, fs + 1);
% angVelBody(timeStamp4:timeStamp5,2) = (2*pi)/5;
% angVelBody(timeStamp5:timeStamp6,2) = (2*pi)/2;
% angVelBody(timeStamp6:timeStamp7,2) = linspace( pi/2, -pi/40, fs + 1);
% angVelBody(timeStamp7:timeStamp10,2) = sin(linspace( 0, 10*pi, 3*fs + 1));
% % angVelBody(timeStamp8:timeStamp9,1) = (2*pi)/40;
% % angVelBody(timeStamp9:timeStamp10,1) = -(2*pi)/3;
% angVelBody(timeStamp10:timeStamp11,2) = -(2*pi)/40;
% angVelBody(timeStamp11:end,2) = 0;

angVelBody(1:timeStamp3,3) = (2*pi)/6;
% angVelBody(timeStamp1:timeStamp2,3) = -(2*pi)/40;
% angVelBody(timeStamp2:timeStamp3,3) = 0;
angVelBody(timeStamp3:timeStamp4,3) = -(2*pi)/5;
angVelBody(timeStamp4:timeStamp5,3) = linspace( (2*pi)/5, -pi/1.5, fs + 1);
angVelBody(timeStamp5:timeStamp6,3) = (2*pi)/2;
angVelBody(timeStamp6:timeStamp7,3) = linspace( pi/2, -pi/40, fs + 1);
angVelBody(timeStamp7:timeStamp8,3) = -(2*pi)/40;
angVelBody(timeStamp8:timeStamp9,1) = (2*pi)/40;
angVelBody(timeStamp9:timeStamp10,1) = -(2*pi)/3;
angVelBody(timeStamp10:timeStamp11,3) = -sin(linspace( 0, 5*pi, fs + 1));
angVelBody(timeStamp11:end,3) = 6*cos(linspace( 0, 10*pi, fs + 1));

[~,orientationNED,~,accNED,angVelNED] = traj(accBody,angVelBody);

IMU = imuSensor('accel-gyro-mag','SampleRate',fs);

IMU.Gyroscope = gyroparams( ...
    'MeasurementRange',4.3633, ...
    'Resolution',0.00013323, ...
    'AxesMisalignment',2, ...
    'NoiseDensity',8.7266e-04, ...
    'TemperatureBias',0.34907, ...
    'TemperatureScaleFactor',0.02, ...
    'AccelerationBias',0.00017809, ...,
    'RandomWalk', 1e-03, ...
    'BiasInstability', [0.02, 0.02, 0.02], ...
    'ConstantBias',[0.3,0.3,0.3]);

IMU.Accelerometer = accelparams( ...
    'MeasurementRange',19.62, ...            % m/s^2
    'Resolution',0.0023936, ...              % m/s^2 / LSB
    'AxesMisalignment',2, ...
    'TemperatureScaleFactor',0.008, ...      % % / degree C
    'ConstantBias',0.1962, ...               % m/s^2
    'BiasInstability', [0.02, 0.02, 0.02], ...
    'TemperatureBias',0.0014715, ...         % m/s^2 / degree C
    'RandomWalk', 1e-03, ...                 % m/s^2 / Hz^(1/2)
    'NoiseDensity',0.0022361);                 % m/s^2 / Hz^(1/2)

IMU.Magnetometer = magparams( ...
    'MeasurementRange',1200, ...             % uT
    'Resolution',0.1, ...                    % uT / LSB
    'TemperatureScaleFactor',0.1, ...        % % / degree C
    'ConstantBias',1, ...                    % uT
    'TemperatureBias',[0.8 0.8 2.4], ...     % uT / degree C
    'NoiseDensity',[0.6 0.6 0.9]/sqrt(100)); % uT / Hz^(1/2)

[accelReading,gyroReading,magReading] = IMU(accNED,angVelNED,orientationNED);

orientationEuler = eulerd(orientationNED,'XYZ','frame');


mat = [orientationEuler gyroReading accelReading magReading];
dlmwrite('orientation.txt',mat)

t = (0:(totalNumSamples-1))/fs;
subplot(4,1,1)
plot(t,orientationEuler)
legend('X-axis','Y-axis','Z-axis')
title('True orientation')
ylabel('oreintation (degrees)')

subplot(4,1,2)
plot(t,gyroReading)
legend('X-axis','Y-axis','Z-axis')
title('Gyroscope Readings')
ylabel('Angular Velocity (rad/s)')

subplot(4,1,3)
plot(t,magReading)
legend('X-axis','Y-axis','Z-axis')
title('Magnetometer Readings')
xlabel('Time (s)')
ylabel('Magnetic Field (uT)')

t = (0:(totalNumSamples-1))/fs;
subplot(4,1,4)
plot(t,accelReading)
legend('X-axis','Y-axis','Z-axis')
title('Accelerometer Readings')
ylabel('Acceleration (m/s^2)')