fs = 100;
firstLoopNumSamples = fs*4;
secondLoopNumSamples = fs*2;
totalNumSamples = firstLoopNumSamples + secondLoopNumSamples;

traj = kinematicTrajectory('SampleRate',fs);

accBody = zeros(totalNumSamples,3);
angVelBody = zeros(totalNumSamples,3);
angVelBody(1:firstLoopNumSamples,1) = (2*pi)/40;
angVelBody(firstLoopNumSamples+1:end,1) = -(2*pi)/20;

angVelBody(1:firstLoopNumSamples,2) = 0;
angVelBody(firstLoopNumSamples+1:end,2) = -(pi)/30;

[~,orientationNED,~,accNED,angVelNED] = traj(accBody,angVelBody);

IMU = imuSensor('accel-gyro-mag','SampleRate',fs);

IMU.Gyroscope = gyroparams( ...
    'MeasurementRange',4.3633, ...
    'Resolution',0.00013323, ...
    'AxesMisalignment',2, ...
    'NoiseDensity',8.7266e-04, ...
    'TemperatureBias',0.34907, ...
    'TemperatureScaleFactor',0.02, ...
    'AccelerationBias',0.00017809, ...,
    'RandomWalk', 1e-03, ...
    'BiasInstability', [0.02, 0.02, 0.02], ...
    'ConstantBias',[0.3,0.3,0.3]);

IMU.Accelerometer = accelparams( ...
    'MeasurementRange',19.62, ...            % m/s^2
    'Resolution',0.0023936, ...              % m/s^2 / LSB
    'AxesMisalignment',2, ...
    'TemperatureScaleFactor',0.008, ...      % % / degree C
    'ConstantBias',0.1962, ...               % m/s^2
    'BiasInstability', [0.02, 0.02, 0.02], ...
    'TemperatureBias',0.0014715, ...         % m/s^2 / degree C
    'RandomWalk', 1e-03, ...                 % m/s^2 / Hz^(1/2)
    'NoiseDensity',0.0022361);                 % m/s^2 / Hz^(1/2)

% IMU.Magnetometer = magparams( ...
%     'MeasurementRange',1200, ...             % uT
%     'Resolution',0.1, ...                    % uT / LSB
%     'TemperatureScaleFactor',0.1, ...        % % / degree C
%     'ConstantBias',1, ...                    % uT
%     'TemperatureBias',[0.8 0.8 2.4], ...     % uT / degree C
%     'NoiseDensity',[0.6 0.6 0.9]/sqrt(100)); % uT / Hz^(1/2)

[accelReading,gyroReading,magReading] = IMU(accNED,angVelNED,orientationNED);

orientationEuler = eulerd(orientationNED,'XYZ','frame');


mat = [orientationEuler gyroReading accelReading magReading];
dlmwrite('orientation.txt',mat)

t = (0:(totalNumSamples-1))/fs;
subplot(4,1,1)
plot(t,orientationEuler)
legend('X-axis','Y-axis','Z-axis')
title('True orientation')
ylabel('oreintation (degrees)')

subplot(4,1,2)
plot(t,gyroReading)
legend('X-axis','Y-axis','Z-axis')
title('Gyroscope Readings')
ylabel('Angular Velocity (rad/s)')

subplot(4,1,3)
plot(t,magReading)
legend('X-axis','Y-axis','Z-axis')
title('Magnetometer Readings')
xlabel('Time (s)')
ylabel('Magnetic Field (uT)')

t = (0:(totalNumSamples-1))/fs;
subplot(4,1,4)
plot(t,accelReading)
legend('X-axis','Y-axis','Z-axis')
title('Accelerometer Readings')
ylabel('Acceleration (m/s^2)')