/*
 * mahony.h
 *
 *  Created on: 18 kwi 2021
 *      Author: marci
 */

#ifndef INC_MAHONY_H_
#define INC_MAHONY_H_

#include <math.h>
#include "pololu_imu.h"

typedef struct ang{
	float roll;
	float pitch;
	float yaw;
}EulerAngles_t;

void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz, float deltat);
EulerAngles_t GetEulerAngles();
void ScaleData(IMU_data_t * data);
float getHeading(float mx, float my, float mz, float ax, float ay, float az);

#endif /* INC_MAHONY_H_ */
