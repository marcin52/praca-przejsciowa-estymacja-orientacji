/*
 * madgwick.h
 *
 *  Created on: 19 kwi 2021
 *      Author: marci
 */

#ifndef INC_MADGWICK_H_
#define INC_MADGWICK_H_

#include <math.h>
#include "pololu_imu.h"
#include "mahony.h"

void MadgwickFilterUpdate(float w_x, float w_y, float w_z, float a_x,
		float a_y, float a_z, float m_x, float m_y, float m_z);
EulerAngles_t MadgwickGetEulerAngles();

#endif /* INC_MADGWICK_H_ */
