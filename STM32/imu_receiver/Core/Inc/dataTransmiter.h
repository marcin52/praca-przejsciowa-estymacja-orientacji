/*
 * dataTransmiter.h
 *
 *  Created on: 3 kwi 2021
 *      Author: marci
 */

#ifndef INC_DATATRANSMITER_H_
#define INC_DATATRANSMITER_H_

#include "usart.h"
#include "pololu_imu.h"

void TransmitImuData(IMU_data_t * data, uint16_t timeStamp);

#endif /* INC_DATATRANSMITER_H_ */
