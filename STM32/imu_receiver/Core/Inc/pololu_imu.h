/*
 * pololu_imu.h
 *
 *  Created on: 27 mar 2021
 *      Author: marcin
 *
 */
#ifndef POLOLU_IMU_H_INCLUDED
#define POLOLU_IMU_H_INCLUDED

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include <stdint.h>
#include <stdbool.h>


#include "main.h"
#include "stm32l4xx_hal.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

#define I2C_WAIT_TIME 						10
#define CALIBRATION_POINTS 					6400
#define LSM6DS33_WHO_AM_I					0x69 // 105 in dec
#define LIS3MDL_WHO_AM_I					0x3D // 61 in dec

/* LSM6DS33 REGISTERS (GYROSCOPE AND ACELEROMETER ) */
/*												   */

#define LSM6DS33_adress 					(0x6B<<1) // adres czujnika: 1101 010x
#define LSM6DS33_whoami_register 			0x0F // rejestr who am i

#define LSM303_ACC_Z_L_A_MULTI_READ 		0x80 // czytanie wielu rejestr�w na raz 1000 0000

#define LSM6DS33_TEMPERATURE_L 				0x20 // rejsetr temperatury czujnika
#define LSM6DS33_TEMPERATURE_H 				0x21

#define LSM6DS33_CTRL4_C_REGISTER			0x13

// CTRL4_c
// General config
#define LSM6DS33_ACC_CHANGE_AF_BW 			0x80  // 1000 0000



/* GYROSCOPE */

#define LSM6DS33_CTRL2_GYRO_register 		0x11 // gyro settings register
#define LSM6DS33_CTRL7_GYRO_REGISTER		0x16 // high-pass filter settings register
#define LSM6DS33_GYRO_OUTX_L_G 				0x22 // rejestr bajtu nizszego osi x
#define LSM6DS33_GYRO_OUTX_H_G 				0x23 // rejestr bajtu wyzszego osi x
#define LSM6DS33_GYRO_OUTY_L_G 				0x24 // rejestr b0ajtu nizszego osi y
#define LSM6DS33_GYRO_OUTY_H_G 				0x25 // rejestr bajtu wyzszego osi y
#define LSM6DS33_GYRO_OUTZ_L_G 				0x26 // rejestr bajtu nizszego osi z
#define LSM6DS33_GYRO_OUTZ_H_G 				0x27 // rejestr bajtu wyzszego osi z


/* GYRO BIT MASKS */
// CTRL2_G = [ODR_G3][ODR_G2][ODR_G1][ODR_G0][ FS_G1][ FS_G0][ FS_125][0]
// Measurement frequencyfull-scale spectrum
#define LSM6DS33_GYRO_125dps 				0x2  // 0000 0010
#define LSM6DS33_GYRO_245dps 				0x0  // 0000 0000
#define LSM6DS33_GYRO_500dps 				0x4  // 0000 0100
#define LSM6DS33_GYRO_1000dps 				0x8  // 0000 1000
#define LSM6DS33_GYRO_2000dps 				0xC  // 0000 1100
#define LSM6DS33_GYRO_1660Hz 				0x80 // 1000 0000
#define LSM6DS33_GYRO_833Hz 				0x70 // 0111 0000
#define LSM6DS33_GYRO_416Hz 				0x60 // 0110 0000
#define LSM6DS33_GYRO_208Hz 				0x50 // 0101 0000
#define LSM6DS33_GYRO_104Hz  				0x40 // 0100 0000
#define LSM6DS33_GYRO_52Hz  				0x30 // 0011 0000
#define LSM6DS33_GYRO_26Hz  				0x20 // 0010 0000
#define LSM6DS33_GYRO_13Hz  				0x10 // 0001 0000

/* GYRO BIT MASKS */
// CTRL7_G = [G_HM_MODE][HP_G_EN][HPCF_G1][HPCF_G0][ HP_G_RST][ ROUNDING_STATUS][ 0][0]
// High-pass filter settings

#define LSM6DS33_GYRO_HIGH_PASS_ENEABLE	    0x40 // 0100 0000
#define LSM6DS33_GYRO_HIGH_PASS_RESET	    0x08 // 0000 1000
#define LSM6DS33_GYRO_ROUNDING_STATUS		0x04 // 0000 0100
#define LSM6DS33_GYRO_HP_CUTOFF_0081Hz		0x00 // 0000 0000
#define LSM6DS33_GYRO_HP_CUTOFF_0324Hz		0x10 // 0001 0000
#define LSMDS33_GYRO_HP_CUTOFF_207Hz		0x20 // 0010 0000
#define LSMDS33_GYRO_HP_CUTOFF_1632Hz		0x30 // 0011 0000

/* AKCELEROMETER*/

#define LSM6DS33_CTRL1_ACC_register 		0x10 // acelerometer settings register
#define LSM6DS33_CTRL8_ACC_register 		0x17 // acelerometer filter settings register
#define LSM6DS33_ACC_OUTX_L_A 				0x28 // rejestr bajtu ni�szego osi x
#define LSM6DS33_ACC_OUTX_H_A 				0x29 //rejestr bajtu wy�szego osi x
#define LSM6DS33_ACC_OUTY_L_A 				0x2A //rejestr bajtu ni�szego osi y
#define LSM6DS33_ACC_OUTY_H_A 				0x2B // rejestr bajtu wy�szego osi y
#define LSM6DS33_ACC_OUTZ_L_A 				0x2C //rejestr bajtu ni�szego osi z
#define LSM6DS33_ACC_OUTZ_H_A 				0x2D // rejestr bajtu wy�szego osi z

/* AKCELEROMETER BIT MASKS */
// CTRL1_A = [ODR_XL3][ODR_XL2][ODR_XL1][ODR_XL0][FS_XL1][FS_XL0][BW_XL1][BW_XL0]
// measurment frequency, full-scale spectrum, anty-aliasing
#define LSM6DS33_ACC_6660Hz 				0xA0 // 1010 0000
#define LSM6DS33_ACC_3330Hz 				0x90 // 1001 0000
#define LSM6DS33_ACC_1660Hz 				0x80 // 1000 0000
#define LSM6DS33_ACC_833Hz 					0x70 // 0111 0000
#define LSM6DS33_ACC_416Hz 					0x60 // 0110 0000
#define LSM6DS33_ACC_208Hz 					0x50 // 0101 0000
#define LSM6DS33_ACC_104Hz  				0x40 // 0100 0000
#define LSM6DS33_ACC_52Hz  					0x30 // 0011 0000
#define LSM6DS33_ACC_26Hz  					0x20 // 0010 0000
#define LSM6DS33_ACC_13Hz  					0x10 // 0001 0000
#define LSM6DS33_ACC_2g  					0x00 // 0000 0000
#define LSM6DS33_ACC_4g 					0x08 // 0000 1000
#define LSM6DS33_ACC_8g 					0x0C // 0000 1100
#define LSM6DS33_ACC_16g  					0x04 // 0000 0100
#define LSM6DS33_ACC_af50Hz  				0x03 // 0000 0011
#define LSM6DS33_ACC_af100Hz  				0x02 // 0000 0010
#define LSM6DS33_ACC_af200Hz 				0x01 // 0000 0001
#define LSM6DS33_ACC_af400Hz 				0x00 // 0000 0000

// CTRL8_A
// filter settings
#define LSM6DS33_ACC_LowPassF2_EN 			0xA0 // 1000 0000
#define LSM6DS33_ACC_HP_SLOPE_EN 			0xA0 // 1000 0000
#define LSM6DS33_ACC_HP_CUTOFF_ODR_DIV_50	0x00 // 0000 0000
#define LSM6DS33_ACC_HP_CUTOFF_ODR_DIV_100	0x20 // 0010 0000
#define LSM6DS33_ACC_HP_CUTOFF_ODR_DIV_9	0x40 // 0100 0000
#define LSM6DS33_ACC_HP_CUTOFF_ODR_DIV_400	0x60 // 0110 0000


/*	 	 	 	 	 	 	 	 	*/
/* LIS3MDL REGISTERS (MAGNETOMETER ) */
/*									*/

#define LIS3MDL_adress 						(0x1E<<1) // 0011 110
#define LIS3MDL_whoami_register				0x0F // 0011 1101
#define LIS3DML_CTRL_REG1 					0x20 //
#define LIS3DML_CTRL_REG2 					0x21 //
#define LIS3DML_CTRL_REG3 					0x22 //
#define LIS3DML_CTRL_REG4 					0x23 //
#define LIS3DML_INT_CNF 					0x30
#define LIS3MDL_OUT_X_L 					0x28 //LOWER BYTE ON X AXIS
#define LIS3MDL_OUT_X_H 					0x29 //HIGHER BYTE ON X AXIS
#define LIS3MDL_OUT_Y_L 					0x2A //LOWER BYTE ON Y AXIS
#define LIS3MDL_OUT_Y_H 					0x2B //HIGHER BYTE ON Y AXIS
#define LIS3MDL_OUT_Z_L 					0x2C //LOWER BYTE ON Z AXIS
#define LIS3MDL_OUT_Z_H 					0x2D //HGHER BYTE ON Z AXIS
#define LIS3MDL_TEMPERATURE_X_L				0x2E // LOWER TEMPERATURE BYTE
#define LIS3MDL_TEMPERATURE_X_H				0x2F // HIGHER TEMPERATURE BYTE

/* MAGNETOMETER BIT MASKS */

//CTRL_REG1
//OutPut DATA RATE, TEMPERATURE SENSOR, X,Y OPERATIVE MODES

#define LIS3MDL_TEMP_SENS_EN 				0x80 // 1000 0000
#define LIS3MDL_XY_OPER_MODE_LOW 			0x00 // 0000 0000
#define LIS3MDL_XY_OPER_MODE_MEDIUM 		0x20 // 0010 0000
#define LIS3MDL_XY_OPER_MODE_HIGH 			0x40 // 0100 0000
#define LIS3MDL_XY_OPER_MODE_ULTR_HIGH		0x60 // 0110 0000
#define LIS3MDL_DATA_RATE_0_625				0x00 // 0000 0000
#define LIS3MDL_DATA_RATE_1_25				0x04 // 0000 0100
#define LIS3MDL_DATA_RATE_2_5				0x08 // 0000 1000
#define LIS3MDL_DATA_RATE_5					0x0C // 0000 1100
#define LIS3MDL_DATA_RATE_10				0x10 // 0001 0000
#define LIS3MDL_DATA_RATE_20				0x14 // 0001 0100
#define LIS3MDL_DATA_RATE_40				0x18 // 0001 1000
#define LIS3MDL_DATA_RATE_80				0x1C // 0001 1100
#define LIS3MDL_DATA_RATE_FAST_ODR			0x02 // 0000 0010
#define LIS3MDL_SELF_TEST					0x01 // 0000 0001


//CTRL_REG2 = [0][FS1][FS2][0][REBOOT][SOFT_RES][0][0]
//FULL-SCALE CONFIGURATION
#define LIS3MDL_4gauss 						0x00 // 0000 0000
#define LIS3MDL_8gauss 						0x20 // 0010 0000
#define LIS3MDL_12gauss 					0x40 // 0100 0000
#define LIS3MDL_16gauss 					0x60 // 0110 0000


//CTRL_REG3
// LOW_POWER MODE, OPERATING_MODE
#define LIS3MDL_LOW_POWER_MODE		 		0x20 // 0010 0000
#define LIS3MDL_CONTINOUS_CONVERSION 		0x00 // 0000 0000
#define LIS3MDL_SINGLE_CONVERSION 			0x01 // 0000 0001
#define LIS3MDL_POWER_DOWN 					0x03 // 0000 0011


//CTRL_REG4
//Z_AZIS OPERATING MODE
#define LIS3MDL_Z_OPER_MODE_LOW 			0x00 // 0000 0000
#define LIS3MDL_Z_OPER_MODE_MEDIUM 			0x04 // 0000 0100
#define LIS3MDL_Z_OPER_MODE_HIGH 			0x08 // 0000 1000
#define LIS3MDL_Z_OPER_MODE_ULTR_HIGH		0x0C // 0000 1100


/*******************************************************************************
 *    PUBLIC TYPES
 ******************************************************************************/


/* struct that stores all values read from imu registers. */

typedef struct gyro_conf{

	uint8_t outputDataRate;
	uint8_t fullScaleSpectrum;
	bool highPassFilterEnable;
	uint8_t highPassFilterFreq;

}IMU_GyroConfig_t;

typedef struct acc_conf{

	uint8_t outputDataRate;
	uint8_t fullScaleSpectrum;
	bool analogFilterEnable;
	uint8_t analogFilterBW;
	bool lpf2FilterEnable;
	bool slopeHighPassFilterEnable;
	uint8_t slopeHighPassFilterCutoff;

}IMU_AccConf_t;

typedef struct magn_conf{

	uint8_t fullScaleSpectrum;
	bool lowPowerMode;
	uint8_t conversionMode;
	uint8_t zAxisMode;
	uint8_t xyAxisMode;
	bool tempSensEnable;
	bool fastODR;
	uint8_t slowODR_value;

}IMU_MagnConf_t;

typedef struct imu_config
{
	IMU_GyroConfig_t gyroConfig;
	IMU_AccConf_t accConfig;
	IMU_MagnConf_t magnConfig;

}IMU_Config_t;

typedef struct imuRawData{
	uint8_t gyrox_l;
	uint8_t gyrox_h;
	uint8_t gyroy_l;
	uint8_t gyroy_h;
	uint8_t gyroz_l;
	uint8_t gyroz_h;
	uint8_t accx_l;
	uint8_t accx_h;
	uint8_t accy_l;
	uint8_t accy_h;
	uint8_t accz_l;
	uint8_t accz_h;
	uint8_t magnx_l;
	uint8_t magnx_h;
	uint8_t magny_l;
	uint8_t magny_h;
	uint8_t magnz_l;
	uint8_t magnz_h;
	uint8_t temp_magn_l;
	uint8_t temp_magn_h;
	uint8_t temp_acc_gyro_l;
	uint8_t temp_acc_gyro_h;
}IMU_raw_data_t;

typedef struct imu
{
	IMU_raw_data_t rawData;
	int16_t gyro_x;
	int16_t gyro_y;
	int16_t gyro_z;
	int16_t accel_x;
	int16_t accel_y;
	int16_t accel_z;
	int16_t magn_x;
	int16_t magn_y;
	int16_t magn_z;
	float sc_gyro_x;
	float sc_gyro_y;
	float sc_gyro_z;
	float sc_accel_x;
	float sc_accel_y;
	float sc_accel_z;
	float sc_magn_x;
	float sc_magn_y;
	float sc_magn_z;
	int16_t accGyroTemp;
	float accGyroTemperatureInCelc;
	int16_t magnTemp;
	float magnTemperatureInCelc;
	uint32_t time;

}IMU_data_t;

/*******************************************************************************
 *    GLOBAL VARIABLES
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTION PROTOTYPES
 ******************************************************************************/

/**
 * Basic initialization.
 * @param hi2c - hal i2c handle typedef
 */
void IMU_init(IMU_data_t * imu, IMU_Config_t * imuConfig);
void IMU_gyro_init( IMU_GyroConfig_t * gyroConfig );
void IMU_accel_init( IMU_AccConf_t * accConfig );
void IMU_magn_init(IMU_MagnConf_t * magnConfig);
bool IMU_test();

void IMU_AccGyroTemperatureRead( IMU_data_t * imu_data, bool calculateInCels);
void IMU_MagnTemperatureRead( IMU_data_t * imu_data, bool calculateInCels);

void IMU_gyro_read_x( IMU_data_t * imu_data);
void IMU_gyro_read_y( IMU_data_t * imu_data);
void IMU_gyro_read_z( IMU_data_t * imu_data);
void IMU_gyro_read_all( IMU_data_t * imu_data);

void IMU_acc_read_x( IMU_data_t * imu_data);
void IMU_acc_read_y( IMU_data_t * imu_data);
void IMU_acc_read_z( IMU_data_t * imu_data);
void IMU_acc_read_all( IMU_data_t * imu_data);

void IMU_magn_read_x( IMU_data_t * imu_data);
void IMU_magn_read_y( IMU_data_t * imu_data);
void IMU_magn_read_z( IMU_data_t * imu_data);
void IMU_magn_read_all( IMU_data_t * imu_data);

void IMU_resetGyroHighPassFilter();

#endif //POLOLU_IMU

/*******************************************************************************
 *    end of file
 ******************************************************************************/
