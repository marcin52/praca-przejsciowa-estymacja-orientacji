/*
 * dataTransmiter.c
 *
 *  Created on: 3 kwi 2021
 *      Author: marci
 */

#include "dataTransmiter.h"

static uint8_t messageStartChar = '$';
static uint8_t messageStopChar = '(';
static uint8_t messageLenght = 26;

void TransmitImuData(IMU_data_t * data, uint16_t timeStamp){

	volatile uint8_t dataToSend[messageLenght];

	uint8_t timeStampLowByte = timeStamp & 0xFF;
	uint8_t timeStampHighByte = (timeStamp >> 8) & 0xFF;

	dataToSend[0] = messageStartChar;
	dataToSend[1] = data->rawData.gyrox_l;
	dataToSend[2] = data->rawData.gyrox_h;
	dataToSend[3] = data->rawData.gyroy_l;
	dataToSend[4] = data->rawData.gyroy_h;
	dataToSend[5] = data->rawData.gyroz_l;
	dataToSend[6] = data->rawData.gyroz_h;
	dataToSend[7] = data->rawData.accx_l;
	dataToSend[8] = data->rawData.accx_h;
	dataToSend[9] = data->rawData.accy_l;
	dataToSend[10] = data->rawData.accy_h;
	dataToSend[11] = data->rawData.accz_l;
	dataToSend[12] = data->rawData.accz_h;
	dataToSend[13] = data->rawData.magnx_l;
	dataToSend[14] = data->rawData.magnx_h;
	dataToSend[15] = data->rawData.magny_l;
	dataToSend[16] = data->rawData.magny_h;
	dataToSend[17] = data->rawData.magnz_l;
	dataToSend[18] = data->rawData.magnz_h;
	dataToSend[19] = data->rawData.temp_magn_l;
	dataToSend[20] = data->rawData.temp_magn_h;
	dataToSend[21] = data->rawData.temp_acc_gyro_l;
	dataToSend[22] = data->rawData.temp_acc_gyro_h;
	dataToSend[23] = timeStampLowByte;
	dataToSend[24] = timeStampHighByte;
	dataToSend[25] = messageStopChar;

	USART2_WriteBytes(dataToSend, messageLenght);

}
