/*
 * mahony.c
 *
 *  Created on: 18 kwi 2021
 *      Author: marci
 */

#include "mahony.h"

/*********************************
 * SCALING FACTORS
 ********************************/

float M_B[3] = {   -104.636886, -382.993375,   -1188.400987}; //mag offsets and scale
float M_Ainv[3][3] =
{ { 0.000270,  0.000025,  0.000010},
  {  0.000025,  0.000299, -0.000006},
  { 0.000010, -0.000006,  0.000285}
};

float A_B[3] = { 160.592444, -108.505471, -19.180516}; //accelerometer offsets and scale
float A_Ainv[3][3] =
{ {  0.000063,  0.000002, 0.000002},
  {  0.000002,  0.000062, -0.000001},
  {  0.000002, -0.000001, 0.000058}
};

//The L3GD20H default full scale setting is +/- 245 dps.
#define gscale 8.75e-3*(M_PI/180.0)  //gyro default 8.75 mdps -> rad/s

//float G_off[3] = {83.7, -293.8, -380.5}; //raw offsets, determined for gyro at rest
float G_off[3] = {450.7, -759.9, -224.4}; //raw offsets, determined for gyro at rest

//raw data and scaled as vector
float Axyz[3];
float Gxyz[3];
float Mxyz[3];

float m_off[3] = {-826,   -493,  -1724};
float m_scl[3] = { +3461,  +3317,  +3531};

/*********************************
 * MAHONY FILTER VARIABLES
 ********************************/

// NOW USING MAHONY FILTER

// These are the free parameters in the Mahony filter and fusion scheme,
// Kp for proportional feedback, Ki for integral
// with MPU-9250, angles start oscillating at Kp=40. Ki does not seem to help and is not required.
#define Kp 30.0
#define Ki 0.0001

// Vector to hold quaternion
static float q[4] = {1.0, 0.0, 0.0, 0.0};
//static float yaw, pitch, roll; //Euler angle output
static EulerAngles_t angles = {0, 0, 0};

/*********************************
 * PRIVATE FUNCTIONS PROTOTYPES
 ********************************/

float vector_dot(float a[3], float b[3]);
void vector_normalize(float a[3]);
void vector_var_normalize(float *a, float *b, float *c);
void vector_cross(const float *a, const float *b, float *out);
/****************************************
 * PUBLIC FUNCTIONS
 *****************************************/

// Mahony scheme uses proportional and integral filtering on
// the error between estimated reference vectors and measured ones.
void MahonyQuaternionUpdate(float ax, float ay, float az, float gx, float gy, float gz, float mx, float my, float mz, float deltat)
{
 // Vector to hold integral error for Mahony method
  static float eInt[3] = {0.0, 0.0, 0.0};
// short name local variable for readability
  float q1 = q[0], q2 = q[1], q3 = q[2], q4 = q[3];
  float norm;
  float hx, hy, bx, bz;
  float vx, vy, vz, wx, wy, wz;
  float ex, ey, ez;
  float pa, pb, pc;

  // Auxiliary variables to avoid repeated arithmetic
  float q1q1 = q1 * q1;
  float q1q2 = q1 * q2;
  float q1q3 = q1 * q3;
  float q1q4 = q1 * q4;
  float q2q2 = q2 * q2;
  float q2q3 = q2 * q3;
  float q2q4 = q2 * q4;
  float q3q3 = q3 * q3;
  float q3q4 = q3 * q4;
  float q4q4 = q4 * q4;

    // already done in loop()

    // Normalise accelerometer measurement
//    norm = sqrt(ax * ax + ay * ay + az * az);
//    if (norm == 0.0f) return; // Handle NaN
//    norm = 1.0f / norm;       // Use reciprocal for division
//    ax *= norm;
//    ay *= norm;
//    az *= norm;
/*
    // Normalise magnetometer measurement
    norm = sqrt(mx * mx + my * my + mz * mz);
    if (norm == 0.0f) return; // Handle NaN
    norm = 1.0f / norm;       // Use reciprocal for division
    mx *= norm;
    my *= norm;
    mz *= norm;
  */
  // Reference direction of Earth's magnetic field
  hx = 2.0f * mx * (0.5f - q3q3 - q4q4) + 2.0f * my * (q2q3 - q1q4) + 2.0f * mz * (q2q4 + q1q3);
  hy = 2.0f * mx * (q2q3 + q1q4) + 2.0f * my * (0.5f - q2q2 - q4q4) + 2.0f * mz * (q3q4 - q1q2);
  bx = sqrt((hx * hx) + (hy * hy));
  bz = 2.0f * mx * (q2q4 - q1q3) + 2.0f * my * (q3q4 + q1q2) + 2.0f * mz * (0.5f - q2q2 - q3q3);

  // Estimated direction of gravity and magnetic field
  vx = 2.0f * (q2q4 - q1q3);
  vy = 2.0f * (q1q2 + q3q4);
  vz = q1q1 - q2q2 - q3q3 + q4q4;
  wx = 2.0f * bx * (0.5f - q3q3 - q4q4) + 2.0f * bz * (q2q4 - q1q3);
  wy = 2.0f * bx * (q2q3 - q1q4) + 2.0f * bz * (q1q2 + q3q4);
  wz = 2.0f * bx * (q1q3 + q2q4) + 2.0f * bz * (0.5f - q2q2 - q3q3);

  // Error is cross product between estimated direction and measured direction of gravity
  ex = (ay * vz - az * vy) + (my * wz - mz * wy);
  ey = (az * vx - ax * vz) + (mz * wx - mx * wz);
  ez = (ax * vy - ay * vx) + (mx * wy - my * wx);
  if (Ki > 0.0f)
  {
    eInt[0] += ex;      // accumulate integral error
    eInt[1] += ey;
    eInt[2] += ez;
  }
  else
  {
    eInt[0] = 0.0f;     // prevent integral wind up
    eInt[1] = 0.0f;
    eInt[2] = 0.0f;
  }

  // Apply feedback terms to gyro rate measurement
  gx = gx + Kp * ex + Ki * eInt[0];
  gy = gy + Kp * ey + Ki * eInt[1];
  gz = gz + Kp * ez + Ki * eInt[2];

  // Integrate rate of change of quaternion
  pa = q2;
  pb = q3;
  pc = q4;
  q1 = q1 + (-q2 * gx - q3 * gy - q4 * gz) * (0.5f * deltat);
  q2 = pa + (q1 * gx + pb * gz - pc * gy) * (0.5f * deltat);
  q3 = pb + (q1 * gy - pa * gz + pc * gx) * (0.5f * deltat);
  q4 = pc + (q1 * gz + pa * gy - pb * gx) * (0.5f * deltat);

  // Normalise quaternion
  norm = sqrt(q1 * q1 + q2 * q2 + q3 * q3 + q4 * q4);
  norm = 1.0f / norm;
  q[0] = q1 * norm;
  q[1] = q2 * norm;
  q[2] = q3 * norm;
  q[3] = q4 * norm;
}

EulerAngles_t GetEulerAngles(){

	EulerAngles_t angles;

	angles.roll  = atan2((q[0] * q[1] + q[2] * q[3]), 0.5 - (q[1] * q[1] + q[2] * q[2]));
	angles.pitch = asin(2.0 * (q[0] * q[2] - q[1] * q[3]));
	angles.yaw = atan2((q[1] * q[2] + q[0] * q[3]), 0.5 - ( q[2] * q[2] + q[3] * q[3]));

	// to degrees
	angles.yaw   *= 180.0 / M_PI;
	angles.pitch *= 180.0 / M_PI;
	angles.roll *= 180.0 / M_PI;

	// http://www.ngdc.noaa.gov/geomag-web/#declination
	//conventional nav, yaw increases CW from North, corrected for local magnetic declination
	angles.yaw = -angles.yaw + 8.07;
	if (angles.yaw > 360.0) angles.yaw -= 360.0;
	if (angles.yaw < 0) angles.yaw += 360.0;

	return angles;
}

void ScaleData(IMU_data_t * data) {

	float temp[3];

	//apply offsets (bias) and scale factors from Magneto
//	for (i = 0; i < 3; i++) temp[i] = (Axyz[i] - A_B[i]);
//	Axyz[0] = A_Ainv[0][0] * temp[0] + A_Ainv[0][1] * temp[1] + A_Ainv[0][2] * temp[2];
//	Axyz[1] = A_Ainv[1][0] * temp[0] + A_Ainv[1][1] * temp[1] + A_Ainv[1][2] * temp[2];
//	Axyz[2] = A_Ainv[2][0] * temp[0] + A_Ainv[2][1] * temp[1] + A_Ainv[2][2] * temp[2];
//	vector_normalize(Axyz);

//	for (i = 0; i < 3; i++) temp[i] = (Axyz[i] - A_B[i]);
//	Axyz[0] = A_Ainv[0][0] * temp[0] + A_Ainv[0][1] * temp[1] + A_Ainv[0][2] * temp[2];
//	Axyz[1] = A_Ainv[1][0] * temp[0] + A_Ainv[1][1] * temp[1] + A_Ainv[1][2] * temp[2];
//	Axyz[2] = A_Ainv[2][0] * temp[0] + A_Ainv[2][1] * temp[1] + A_Ainv[2][2] * temp[2];

	Axyz[0] =(float)data->accel_x;
	Axyz[1] =(float)data->accel_y;
	Axyz[2] = (float)data->accel_z;
	vector_normalize(Axyz);
	data->sc_accel_x = Axyz[0];
	data->sc_accel_y = Axyz[1];
	data->sc_accel_z = Axyz[2];
	//vector_normalize(Axyz);

//	//apply offsets (bias) and scale factors from Magneto
	temp[0] = data->magn_x;
	temp[1] = data->magn_y;
	temp[2] = data->magn_z;
	for (int i = 0; i < 3; i++) temp[i] -= M_B[i];
	Mxyz[0] = M_Ainv[0][0] * temp[0] + M_Ainv[0][1] * temp[1] + M_Ainv[0][2] * temp[2];
	Mxyz[1] = M_Ainv[1][0] * temp[0] + M_Ainv[1][1] * temp[1] + M_Ainv[1][2] * temp[2];
	Mxyz[2] = M_Ainv[2][0] * temp[0] + M_Ainv[2][1] * temp[1] + M_Ainv[2][2] * temp[2];
//
//	Mxyz[0] =( (float)temp[0] - m_off[0]) / m_scl[0];
//	Mxyz[1] =( (float)temp[1] - m_off[1]) / m_scl[1];
//	Mxyz[2] =( (float)temp[2] - m_off[2]) / m_scl[2];
	vector_normalize(Mxyz);
	data->sc_magn_x = Mxyz[0];
	data->sc_magn_y = Mxyz[1];
	data->sc_magn_z = Mxyz[2];

	data->sc_gyro_x = ((float)data->gyro_x - G_off[0]) * gscale;
	data->sc_gyro_y = ((float)data->gyro_y - G_off[1]) * gscale;
	data->sc_gyro_z = ((float)data->gyro_z - G_off[2]) * gscale;
}

float getHeading(float mx, float my, float mz, float ax, float ay, float az)
{
	float from[3] = {mx, my, mz};
    float temp_m[3] = {mx, my, mz};
    float a[3] = {ax, ay, az};

    // subtract offset (average of min and max) from magnetometer readings
    temp_m[0] -= m_off[0];
    temp_m[0] -= m_off[1];
    temp_m[0] -= m_off[2];

    // compute E and N
    float E[3] = {0,0,0};
    float N[3] = {0,0,0};
    vector_cross(temp_m, a, E);
    vector_normalize(E);
    vector_cross(a, E, N);
    vector_normalize(N);

    // compute heading
    float heading = atan2(vector_dot(E, from), vector_dot(N, from)) * 180 / M_PI;
    if (heading < 0) heading += 360;
    return heading;
}

/****************************************
 * PRIVATE FUNCTIONS
 *****************************************/

float vector_dot(float a[3], float b[3])
{
  return a[0] * b[0] + a[1] * b[1] + a[2] * b[2];
}

void vector_normalize(float a[3])
{
  float mag = sqrt(vector_dot(a, a));
  if (mag != 0.0) {
	  a[0] /= mag;
	  a[1] /= mag;
	  a[2] /= mag;
  }
}

void vector_var_normalize(float *a, float *b, float *c)
{
  float mag = sqrt(*a * *a + *b * *b + *c * *c);
  *a /= mag;
  *b /= mag;
  *c /= mag;
}

void vector_cross(const float *a, const float *b, float *out)
{
  out[0] = (a[1] * b[2]) - (a[2] * b[1]);
  out[1] = (a[2] * b[0]) - (a[0] * b[2]);
  out[2] = (a[0] * b[1]) - (a[1] * b[0]);
}
