/*
 * sphereFittingAlgorithm.c
 *
 *  Created on: 12.12.2019
 *      Author: marci
 */

/*
# copyright 2012 by Rolfe Schmidt

	This file is part of muCSense.

    muCSense is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 3 of the License, or
    (at your option) any later version.

    muCSense is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with muCSense.  If not, see <http://www.gnu.org/licenses/>.

*/
#include <string.h> //for memset
#include <math.h>

#include "sphereFittingAlgorithm.h"


size_t upperTriangularIndex(size_t i, size_t j);

void update(int16_t * obs) {
  size_t i,j, dataSize;

  //increment sample count
  ++_N;

  int32_t squareObs[3];
  for(i=0; i < 3; ++i) {
    float curObs = obs[i];  //implicitly convert to bigger int
    squareObs[i] = curObs*curObs;  //square of 16-bit int will fit in 32-bit int
  }


  for(i=0; i < 3; ++i) {
    //Keep track of min and max in each dimension
    _obsMin[i] = (obs[i] < _obsMin[i]) ? obs[i] : _obsMin[i];
    _obsMax[i] = (obs[i] > _obsMax[i]) ? obs[i] : _obsMax[i];

    //accumulate sum and sum of squares in each dimension
    _mu[i] += obs[i];
    _mu2[i] += squareObs[i];

    //accumulate inner products of the vector of observations and the vector of squared observations.
    for(j=0;j<3;++j) {
      _ipX2X[i][j] += squareObs[i]*(int32_t)(obs[j]);
      if(i <= j) {
        size_t idx = upperTriangularIndex(i,j);
        _ipXX[idx] += (int32_t)(obs[i])*(int32_t)(obs[j]);
        _ipX2X2[idx] += squareObs[i]*(float)(squareObs[j]);
      }
    }
  }


}


void clearObservationMatrices(){
      _N = 0;
      memset(_mu, 0, 3*sizeof(float));
      memset(_mu2, 0, 3*sizeof(float));
      memset(_ipXX, 0, 6*sizeof(float));
      memset(_ipX2X2, 0, 6*sizeof(float));

      memset(_obsMin, 0, 3*sizeof(int16_t));
      memset(_obsMax, 0, 3*sizeof(int16_t));

      size_t i,j;
      for(i=0;i<3;++i) {
        _obsMin[i] = 0x7fff; //INT16_MAX;
        _obsMax[i] = (-0x7fff - 1); //INT16_MIN;
        for(j=0;j<3;++j) {
          _ipX2X[i][j] = 0.0;
        }
      }
}



void clearGNMatrices(float JtJ[][6], float JtR[]){
  size_t j,k;
    for(j=0;j<6;++j) {
       JtR[j] = 0.0;
       for(k=0;k<6;++k) {
         JtJ[j][k] = 0.0;
       }
    }

}

void transform(const int16_t* rawInput, float* output){
  for(int i=0;i<3;++i) {
    output[i] = ((float)(rawInput[i]) - _beta[i])/_beta[3 + i];
  }
}

void computeGNMatrices(float JtJ[][6], float JtR[]) {
  size_t i,j;

  float beta2[6]; //precompute the squares of the model parameters
  for(i=0;i<6;++i) {
    beta2[i] = pow(_beta[i],2);
  }

  //compute the inner product of the vector of residuals with the constant 1 vector, the vector of
  // observations, and the vector of squared observations.
  float r = _N; //sum of residuals
  float rx[3];  //Inner product of vector of residuals with each observation vector
  float rx2[3];  //Inner product of vector of residuals with each square observation vector


  //now correct the r statistics
  for(i=0;i<3;++i) {
    r -= (beta2[i]*_N + _mu2[i] - 2*_beta[i]*_mu[i])/beta2[3+i];
    rx[i] = _mu[i];
    rx2[i] = _mu2[i];
    for(j=0;j<3;++j) {
      rx[i] -= (beta2[j]*_mu[i] + _ipX2X[j][i] - 2*_ipXX[upperTriangularIndex(i,j)]*_beta[j])/beta2[3+j];
      rx2[i] -= (beta2[j]*_mu2[i] + _ipX2X2[upperTriangularIndex(i,j)] - 2*_ipX2X[i][j]*_beta[j])/beta2[3+j];
    }
  }

  for(i=0;i<3;++i) {
    //Compute product of Jacobian matrix with the residual vector
    JtR[i] = 2*(rx[i] - _beta[i]*r)/beta2[3+i];
    JtR[3+i] = 2*(rx2[i] - 2*_beta[i]*rx[i] + beta2[i]*r)/(beta2[3+i]*_beta[3+i]);

    //Now compute the product of the transpose of the jacobian with itself
    //Start with the diagonal blocks
    for(j=i;j<3;++j) {
      JtJ[i][j] = JtJ[j][i] = 4*(_ipXX[upperTriangularIndex(i,j)] - _beta[i]*_mu[j] - _beta[j]*_mu[i] + _beta[i]*_beta[j]*_N)/(beta2[3+i]*beta2[3+j]);
      JtJ[3+i][3+j] = JtJ[3+j][3+i]
                =  4*(_ipX2X2[upperTriangularIndex(i,j)] - 2*_beta[j]*_ipX2X[i][j] + beta2[j]*_mu2[i]
                       -2*_beta[i]*_ipX2X[j][i] + 4*_beta[i]*_beta[j]*_ipXX[upperTriangularIndex(i,j)] - 2*_beta[i]*beta2[j]*_mu[i]
                       +beta2[i]*_mu2[j] - 2*beta2[i]*_beta[j]*_mu[j] + beta2[i]*beta2[j]*_N)/pow(_beta[3+i]*_beta[3+j], 3);
    }
    //then get the off diagonal blocks
    for(j=0;j<3;++j) {
      JtJ[i][3+j] = JtJ[3+j][i]
          = 4*(_ipX2X[j][i] - 2*_beta[j]*_ipXX[upperTriangularIndex(i,j)] + beta2[j]*_mu[i]
                -_beta[i]*_mu2[j] + 2*_beta[i]*_beta[j]*_mu[j] - _beta[i]*beta2[j]*_N)/(beta2[3+i]*beta2[3+j]*_beta[3+j]);
    }
  }
}


void findDelta(float JtJ[][6], float JtR[]){
  //Solve 6-d matrix equation JtJS*x = JtR
  //first put in upper triangular form
  //Serial.println("find delta");
  int i,j,k;
  double lambda;

  //make upper triangular
  for(i=0;i<6;++i) {
    //eliminate all nonzero entries below JS[i][i

    for(j=i+1;j<6;++j) {
      lambda = JtJ[j][i]/JtJ[i][i];
      if(lambda != 0.0) {
        JtR[j] -= lambda*JtR[i];
        for(k=i;k<6;++k) {
          JtJ[j][k] -= lambda*JtJ[i][k];
        }
      }
    }
  }


  //back-substitute
  for(i=5;i>=0;--i) {
    JtR[i] /= JtJ[i][i];
    JtJ[i][i] = 1.0;
    for(j=0;j<i;++j) {
      lambda = JtJ[j][i];
      JtR[j] -= lambda*JtR[i];
      JtJ[j][i] = 0.0;
    }
  }

}


void calibrate() {
  guessParameters();
  float JtJ[6][6];
  float JtR[6];
  clearGNMatrices(JtJ,JtR);
  //print beta

  int i;

  float eps = 0.000000001;
  int num_iterations = 20;
  float change = 100.0;
  while (--num_iterations >=0 && change > eps) {

    computeGNMatrices(JtJ, JtR);
    findDelta(JtJ, JtR);

    change = JtR[0]*JtR[0] + JtR[1]*JtR[1] + JtR[2]*JtR[2] + JtR[3]*JtR[3]*(_beta[3]*_beta[3]) + JtR[4]*JtR[4]*(_beta[4]*_beta[4]) + JtR[5]*JtR[5]*(_beta[5]*_beta[5]);

    if (!isnan(change)) {
      for(i=0;i<6;++i) {
        _beta[i] -= JtR[i];
      }

      if(i >=3) { _beta[i] = fabs(_beta[i]); }
    }

    clearGNMatrices(JtJ,JtR);

  }

}


size_t upperTriangularIndex(size_t i, size_t j) {
  if (i > j) {
    size_t temp = i;
    i = j;
    j = temp;
  }

  return (j*(j+1))/2 + i;
}

void  guessParameters() {
    for(int i=0;i<3;++i) {
      _beta[i] = ((double)(_obsMax[i] + _obsMin[i])) / 2.0;
      _beta[3+i] = ((double)(_obsMax[i] - _obsMin[i]))/2.0;
  }
}



