/*
 * pololu_imu.c
 *
 *  Created on: 27 mar 2021
 *      Author: marcin
 */


/*******************************************************************************
 *    Description:   See header file
 *
 ******************************************************************************/

/*******************************************************************************
 *    INCLUDED FILES
 ******************************************************************************/

#include "pololu_imu.h"
#include <limits.h>
#include "i2c.h"

/*******************************************************************************
 *    DEFINITIONS
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE FUNCTION PROTOTYPES
 ******************************************************************************/

static void i2c_writeRegister(uint8_t deviceAddress, uint8_t regAdd, uint8_t value);
static uint8_t i2c_readRegister(uint8_t deviceAddress, uint8_t regAdd);

static void magn_writeCTRL1_reg(IMU_MagnConf_t * magnConfig);
static void magn_writeCTRL2_reg(IMU_MagnConf_t * magnConfig);
static void magn_writeCTRL3_reg(IMU_MagnConf_t * magnConfig);
static void magn_writeCTRL4_reg(IMU_MagnConf_t * magnConfig);

/*******************************************************************************
 *    PRIVATE TYPES
 ******************************************************************************/

/*******************************************************************************
 *    PRIVATE DATA
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL DATA
 ******************************************************************************/

/*******************************************************************************
 *    EXTERNAL FUNCTION PROTOTYPES
 ******************************************************************************/

/*******************************************************************************
 *    PROTECTED FUNCTIONS
 ******************************************************************************/

/*******************************************************************************
 *    PUBLIC FUNCTIONS
 ******************************************************************************/

void IMU_init(IMU_data_t * imu, IMU_Config_t * imuConfig){

	MX_I2C1_Init();

	IMU_gyro_init( &(imuConfig->gyroConfig) );
	IMU_accel_init( &(imuConfig->accConfig) );
	IMU_magn_init( &(imuConfig->magnConfig) );

}

void IMU_gyro_init( IMU_GyroConfig_t * gyroConfig ){

	uint8_t settings = gyroConfig->fullScaleSpectrum
				| gyroConfig->outputDataRate;

	i2c_writeRegister(LSM6DS33_adress, LSM6DS33_CTRL2_GYRO_register, settings);

	if( gyroConfig->highPassFilterEnable ){

		uint8_t filterSettings = gyroConfig->highPassFilterFreq;
		i2c_writeRegister(LSM6DS33_adress, LSM6DS33_CTRL7_GYRO_REGISTER, filterSettings);
	}
}

void IMU_accel_init(IMU_AccConf_t * accConfig ){

	uint8_t settings = accConfig->fullScaleSpectrum
				| accConfig->outputDataRate;

	uint8_t filterSettings = 0;

	if( accConfig->analogFilterEnable ){

		uint8_t ctrl4RegValue = i2c_readRegister(LSM6DS33_adress, LSM6DS33_CTRL4_C_REGISTER);

		ctrl4RegValue |= LSM6DS33_ACC_CHANGE_AF_BW;

		i2c_writeRegister(LSM6DS33_adress, LSM6DS33_CTRL4_C_REGISTER, ctrl4RegValue);

		settings |= accConfig->analogFilterBW;
	}

	if( accConfig->lpf2FilterEnable ){

		settings |= LSM6DS33_ACC_LowPassF2_EN;

	}

	if( accConfig->slopeHighPassFilterEnable ){

		settings |= LSM6DS33_ACC_HP_SLOPE_EN | accConfig->slopeHighPassFilterCutoff;
	}


	i2c_writeRegister(LSM6DS33_adress, LSM6DS33_CTRL1_ACC_register, settings);
	i2c_writeRegister(LSM6DS33_adress, LSM6DS33_CTRL8_ACC_register, filterSettings);

}


void IMU_magn_init(IMU_MagnConf_t * magnConfig){

	magn_writeCTRL1_reg(magnConfig);
	magn_writeCTRL2_reg(magnConfig);
	magn_writeCTRL3_reg(magnConfig);
	magn_writeCTRL4_reg(magnConfig);

}

bool IMU_test(){

	if( LSM6DS33_WHO_AM_I !=  i2c_readRegister(LSM6DS33_adress, LSM6DS33_whoami_register) )
		return false;

	else if( LIS3MDL_WHO_AM_I !=  i2c_readRegister(LIS3MDL_adress, LIS3MDL_whoami_register) )
		return false;
	else
		return true;
}

void IMU_AccGyroTemperatureRead(IMU_data_t * imu_data, bool calculateInCels){

	imu_data->rawData.temp_acc_gyro_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_TEMPERATURE_H);
	imu_data->rawData.temp_acc_gyro_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_TEMPERATURE_L );

	imu_data->accGyroTemp = (imu_data->rawData.temp_acc_gyro_h << 8) + imu_data->rawData.temp_acc_gyro_l;

	if( calculateInCels){
		imu_data->accGyroTemperatureInCelc = 25.0 + (float)imu_data->accGyroTemp/50;
	}
}

void IMU_MagnTemperatureRead( IMU_data_t * imu_data, bool calculateInCels){

	imu_data->rawData.temp_magn_l = i2c_readRegister(LIS3MDL_adress, LIS3MDL_TEMPERATURE_X_L);
	imu_data->rawData.temp_magn_h = i2c_readRegister(LIS3MDL_adress, LIS3MDL_TEMPERATURE_X_H );

	imu_data->magnTemp = (imu_data->rawData.temp_magn_h << 8) + imu_data->rawData.temp_magn_l;

	if( calculateInCels){
		imu_data->magnTemperatureInCelc = 25.0 + (float)imu_data->magnTemp/50;
	}
}

void IMU_gyro_read_x( IMU_data_t * imu_data){

	imu_data->rawData.gyrox_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_GYRO_OUTX_L_G);
	imu_data->rawData.gyrox_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_GYRO_OUTX_H_G);

	imu_data->gyro_x = (int16_t)((imu_data->rawData.gyrox_h << 8) + imu_data->rawData.gyrox_l);
}

void IMU_gyro_read_y( IMU_data_t * imu_data){

	imu_data->rawData.gyroy_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_GYRO_OUTY_L_G);
	imu_data->rawData.gyroy_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_GYRO_OUTY_H_G);

	imu_data->gyro_y = (int16_t)((imu_data->rawData.gyroy_h << 8) + imu_data->rawData.gyroy_l);
}

void IMU_gyro_read_z( IMU_data_t * imu_data){

	imu_data->rawData.gyroz_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_GYRO_OUTZ_L_G);
	imu_data->rawData.gyroz_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_GYRO_OUTZ_H_G);

	imu_data->gyro_z = (int16_t)((imu_data->rawData.gyroz_h << 8) + imu_data->rawData.gyroz_l);
}

void IMU_gyro_read_all( IMU_data_t * imu_data){

	IMU_gyro_read_x(imu_data);
	IMU_gyro_read_y(imu_data);
	IMU_gyro_read_z(imu_data);
}

void IMU_acc_read_x( IMU_data_t * imu_data){

	imu_data->rawData.accx_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_ACC_OUTX_L_A);
	imu_data->rawData.accx_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_ACC_OUTX_H_A);

	imu_data->accel_x = (int16_t)((imu_data->rawData.accx_h << 8) + imu_data->rawData.accx_l);
}
void IMU_acc_read_y( IMU_data_t * imu_data){

	imu_data->rawData.accy_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_ACC_OUTY_L_A);
	imu_data->rawData.accy_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_ACC_OUTY_H_A);

	imu_data->accel_y = (int16_t)((imu_data->rawData.accy_h << 8) + imu_data->rawData.accy_l);
}
void IMU_acc_read_z( IMU_data_t * imu_data){

	imu_data->rawData.accz_l = i2c_readRegister(LSM6DS33_adress, LSM6DS33_ACC_OUTZ_L_A);
	imu_data->rawData.accz_h = i2c_readRegister(LSM6DS33_adress, LSM6DS33_ACC_OUTZ_H_A);

	imu_data->accel_z = (int16_t)((imu_data->rawData.accz_h << 8) + imu_data->rawData.accz_l);
}
void IMU_acc_read_all( IMU_data_t * imu_data){

	IMU_acc_read_x(imu_data);
	IMU_acc_read_y(imu_data);
	IMU_acc_read_z(imu_data);
}

void IMU_magn_read_x( IMU_data_t * imu_data){

	imu_data->rawData.magnx_l = i2c_readRegister(LIS3MDL_adress, LIS3MDL_OUT_X_L);
	imu_data->rawData.magnx_h = i2c_readRegister(LIS3MDL_adress, LIS3MDL_OUT_X_H);

	imu_data->magn_x = (int16_t)((imu_data->rawData.magnx_h << 8) + imu_data->rawData.magnx_l);
}

void IMU_magn_read_y( IMU_data_t * imu_data){

	imu_data->rawData.magny_l = i2c_readRegister(LIS3MDL_adress, LIS3MDL_OUT_Y_L);
	imu_data->rawData.magny_h = i2c_readRegister(LIS3MDL_adress, LIS3MDL_OUT_Y_H);

	imu_data->magn_y = (int16_t)((imu_data->rawData.magny_h << 8) + imu_data->rawData.magny_l);
}

void IMU_magn_read_z( IMU_data_t * imu_data){

	imu_data->rawData.magnz_l = i2c_readRegister(LIS3MDL_adress, LIS3MDL_OUT_Z_L);
	imu_data->rawData.magnz_h = i2c_readRegister(LIS3MDL_adress, LIS3MDL_OUT_Z_H);

	imu_data->magn_z = (int16_t)((imu_data->rawData.magnz_h << 8) + imu_data->rawData.magnz_l);
}

void IMU_magn_read_all( IMU_data_t * imu_data){

	IMU_magn_read_x(imu_data);
	IMU_magn_read_y(imu_data);
	IMU_magn_read_z(imu_data);
}

void IMU_resetGyroHighPassFilter(){

	i2c_writeRegister(LSM6DS33_adress, LSM6DS33_CTRL7_GYRO_REGISTER, LSM6DS33_GYRO_HIGH_PASS_RESET);
}

/*******************************************************************************
 *    PRIVATE FUNCTIONS
 ******************************************************************************/

static void i2c_writeRegister(uint8_t deviceAddress, uint8_t regAdd, uint8_t value){

	I2C1_WriteReg(deviceAddress, regAdd, value);

}

static uint8_t i2c_readRegister(uint8_t deviceAddress, uint8_t regAdd){

	return I2C1_ReadReg(deviceAddress, regAdd);

}

static void magn_writeCTRL1_reg(IMU_MagnConf_t * magnConfig){

	uint8_t settings = 0;

	if( magnConfig->fastODR ){
		settings |= LIS3MDL_DATA_RATE_FAST_ODR;
	} else {
		settings |= magnConfig->slowODR_value;
	}

	if( magnConfig->tempSensEnable ){
		settings |= LIS3MDL_TEMP_SENS_EN;
	}

	settings |= magnConfig->xyAxisMode;
	i2c_writeRegister(LIS3MDL_adress, LIS3DML_CTRL_REG1, settings);
}

static void magn_writeCTRL2_reg(IMU_MagnConf_t * magnConfig){

	uint8_t settings = magnConfig->fullScaleSpectrum;

	i2c_writeRegister(LIS3MDL_adress, LIS3DML_CTRL_REG2, settings);
}

static void magn_writeCTRL3_reg(IMU_MagnConf_t * magnConfig){

	uint8_t settings = magnConfig->conversionMode;

	if( magnConfig->lowPowerMode ){
		settings |= LIS3MDL_LOW_POWER_MODE;
	}

	i2c_writeRegister(LIS3MDL_adress, LIS3DML_CTRL_REG3, settings);
}

static void magn_writeCTRL4_reg(IMU_MagnConf_t * magnConfig){

	uint8_t settings = magnConfig->zAxisMode;

	i2c_writeRegister(LIS3MDL_adress, LIS3DML_CTRL_REG4, settings);
}

/*******************************************************************************
 *    end of file
 ******************************************************************************/
